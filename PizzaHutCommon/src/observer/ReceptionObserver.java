package observer;

import dto.TableModel.ReceptionTM;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ReceptionObserver extends Remote {
    void update(ReceptionTM receptionTM)throws RemoteException;
}
