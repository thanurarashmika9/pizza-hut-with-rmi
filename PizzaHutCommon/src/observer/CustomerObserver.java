package observer;

import dto.CustomerDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CustomerObserver extends Remote {
    void update(CustomerDTO customerDTO)throws RemoteException;
}
