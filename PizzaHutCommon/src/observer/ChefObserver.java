package observer;

import dto.CheffDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChefObserver extends Remote {
    void update(CheffDTO cheffDTO)throws RemoteException;

}
