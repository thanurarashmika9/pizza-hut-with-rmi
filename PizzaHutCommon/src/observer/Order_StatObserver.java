package observer;

import dto.Order_StatsDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Order_StatObserver extends Remote {
    void update(Order_StatsDTO order_statsDTO)throws RemoteException;
}
