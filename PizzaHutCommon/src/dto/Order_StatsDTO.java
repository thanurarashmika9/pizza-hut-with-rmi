package dto;

import java.io.Serializable;

public class Order_StatsDTO implements Serializable {
    private String oid;
    private String status;

    public Order_StatsDTO() {
    }

    public Order_StatsDTO(String oid, String status) {
        this.oid = oid;
        this.status = status;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Order_StatsDTO{" +
                "oid='" + oid + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
