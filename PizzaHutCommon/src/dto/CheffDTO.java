package dto;

import java.io.Serializable;

public class CheffDTO implements Serializable {
    private String chid;
    private String name;
    private String address;
    private int contact;
    private double salary;

    public CheffDTO() {
    }

    public CheffDTO(String chid, String name, String address, int contact, double salary) {
        this.chid = chid;
        this.name = name;
        this.address = address;
        this.contact = contact;
        this.salary = salary;
    }

    public String getChid() {
        return chid;
    }

    public void setChid(String chid) {
        this.chid = chid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "CheffDTO{" +
                "chid='" + chid + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", contact=" + contact +
                ", salary=" + salary +
                '}';
    }
}