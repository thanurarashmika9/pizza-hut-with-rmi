package dto.TableModel;

import java.io.Serializable;

public class ReceptionTM implements Serializable {
    private String rid;
    private String name;
    private String address;
    private int contact;
    private double salary;

    public ReceptionTM() {
    }

    public ReceptionTM(String rid, String name, String address, int contact, double salary) {
        this.rid = rid;
        this.name = name;
        this.address = address;
        this.contact = contact;
        this.salary = salary;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "ReceptionTM{" +
                "rid='" + rid + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", contact=" + contact +
                ", salary=" + salary +
                '}';
    }
}
