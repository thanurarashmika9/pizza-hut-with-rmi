package dto.TableModel;

import java.io.Serializable;

public class ItemCategoryTM implements Serializable {
    private String catID;
    private String category;

    public ItemCategoryTM() {
    }

    public ItemCategoryTM(String catID, String category) {
        this.catID=catID;
        this.category=category;
    }

    public String getCatID() {
        return catID;
    }

    public void setCatID(String catID) {
        this.catID = catID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ItemCategoryTM{" +
                "catID='" + catID + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
