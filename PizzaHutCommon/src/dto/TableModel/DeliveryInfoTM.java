package dto.TableModel;

import java.io.Serializable;

public class DeliveryInfoTM implements Serializable {

    private String oid;
    private String Delivery;

    public DeliveryInfoTM() {
    }

    public DeliveryInfoTM(String oid, String delivery) {
        this.oid = oid;
        Delivery = delivery;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getDelivery() {
        return Delivery;
    }

    public void setDelivery(String delivery) {
        Delivery = delivery;
    }

    @Override
    public String toString() {
        return "DeliveryInfoTM{" +
                "oid='" + oid + '\'' +
                ", Delivery='" + Delivery + '\'' +
                '}';
    }
}
