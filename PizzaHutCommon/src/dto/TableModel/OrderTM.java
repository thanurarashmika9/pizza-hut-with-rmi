package dto.TableModel;

public class OrderTM {
    private String cid;
    private String CName;
    private String item;
    private int quantity;
    private double total;

    public OrderTM() {
    }

    public OrderTM(String cid, String CName, String item, int quantity, double total) {
        this.cid = cid;
        this.CName = CName;
        this.item = item;
        this.quantity = quantity;
        this.total = total;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getCName() {
        return CName;
    }

    public void setCName(String CName) {
        this.CName = CName;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "OrderTM{" +
                "cid='" + cid + '\'' +
                ", CName='" + CName + '\'' +
                ", item='" + item + '\'' +
                ", quantity=" + quantity +
                ", total=" + total +
                '}';
    }
}
