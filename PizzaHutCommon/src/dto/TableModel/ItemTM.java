package dto.TableModel;

import java.io.Serializable;

public class ItemTM implements Serializable {
    private String iid;
    private String description;
    private double unit_price;
    private String category;

    public ItemTM() {
    }

    public ItemTM(String iid, String description, double unit_price, String category) {
        this.iid = iid;
        this.description = description;
        this.unit_price = unit_price;
        this.category = category;
    }

    public String getIid() {
        return iid;
    }

    public void setIid(String iid) {
        this.iid = iid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(double unit_price) {
        this.unit_price = unit_price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ItemTM{" +
                "iid='" + iid + '\'' +
                ", description='" + description + '\'' +
                ", unit_price=" + unit_price +
                ", category='" + category + '\'' +
                '}';
    }
}
