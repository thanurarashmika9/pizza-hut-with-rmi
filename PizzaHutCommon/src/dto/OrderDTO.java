package dto;

import java.io.Serializable;
import java.sql.Date;

public class OrderDTO implements Serializable {
    private String oid;
    private Date oDate;
    private String cid;
    private String iid;
    private double price;
    private int qty;
    private String rid;
    private String chid;
    private String did;
    private double total;

    public OrderDTO() {
    }

    public OrderDTO(String oid, Date oDate, String cid, String iid, double price, int qty, String rid, String chid, String did, double total) {
        this.oid = oid;
        this.oDate = oDate;
        this.cid = cid;
        this.iid = iid;
        this.price = price;
        this.qty = qty;
        this.rid = rid;
        this.chid = chid;
        this.did = did;
        this.total = total;
    }

    public OrderDTO(String oid, String cid) {
        this.oid=oid;
        this.cid=cid;
    }

    public OrderDTO(String oid,String cid, String iid, double price, int qty, String rid, String chid, String did, double total) {
        this.oid=oid;
        this.cid=cid;
        this.iid=iid;
        this.price=price;
        this.qty=qty;
        this.rid=rid;
        this.chid=chid;
        this.did=did;
        this.total=total;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public Date getoDate() {
        return oDate;
    }

    public void setoDate(Date oDate) {
        this.oDate = oDate;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getIid() {
        return iid;
    }

    public void setIid(String iid) {
        this.iid = iid;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getChid() {
        return chid;
    }

    public void setChid(String chid) {
        this.chid = chid;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "oid='" + oid + '\'' +
                ", oDate=" + oDate +
                ", cid='" + cid + '\'' +
                ", iid='" + iid + '\'' +
                ", price=" + price +
                ", qty=" + qty +
                ", rid='" + rid + '\'' +
                ", chid='" + chid + '\'' +
                ", did='" + did + '\'' +
                ", total=" + total +
                '}';
    }
}