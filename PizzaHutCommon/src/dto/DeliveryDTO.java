package dto;

import java.io.Serializable;

public class DeliveryDTO implements Serializable {
    private String did;
    private String name;

    public DeliveryDTO() {

    }

    public DeliveryDTO(String did, String name) {
        this.did = did;
        this.name = name;
    }

    public DeliveryDTO(String name) {
        this.name=name;
    }

    public String getDid() {
        return did;
    }

    public void setDid(String did) {
        this.did = did;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DeliveryDTO{" +
                "did='" + did + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
