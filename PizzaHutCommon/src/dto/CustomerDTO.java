package dto;

import java.io.Serializable;

public class CustomerDTO implements Serializable {
    private String cid;
    private String name;
    private String address;
    private int contact;

    public CustomerDTO() {
    }

    public CustomerDTO(String cid, String name, String address, int contact) {
        this.cid = cid;
        this.name = name;
        this.address = address;
        this.contact = contact;
    }

    public CustomerDTO(String cid, String name) {
        this.cid=cid;
        this.name=name;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getContact() {
        return contact;
    }

    public void setContact(int contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "cid='" + cid + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", contact=" + contact +
                '}';
    }
}
