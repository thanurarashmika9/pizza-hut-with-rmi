package controller;

import dto.DeliveryDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.DeliveryInfoTM;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface OrderController extends Remote {

    public boolean makeOrder(OrderDTO orderDTO) throws Exception;

    public ArrayList<OrderDTO>orders(String date) throws Exception;

    public OrderDTO searchOrder(String oid)throws RemoteException;

    public ArrayList<OrderDTO> getOrders() throws Exception;

    public String getOID() throws Exception;

    public OrderDTO getOrder()throws RemoteException;

    int getOrderCount() throws RemoteException;

    ArrayList<OrderDTO> searchByOID(String order) throws Exception;

    ArrayList<OrderDTO> searchByCID(String cid) throws Exception;

    ArrayList<OrderDTO> searchByIID(String iid) throws Exception;

    boolean saveKitchenOrder(OrderDTO order) throws Exception;

    ArrayList<DeliveryDTO> getDeliveryBoys() throws Exception;

    ArrayList<DeliveryInfoTM> getDeliveryStats() throws Exception;

    boolean updateOrder(String oid,String Deliver) throws Exception;

    String getDid(String oid) throws Exception;

    boolean updateCheff(String oid, String chid) throws Exception;
}
