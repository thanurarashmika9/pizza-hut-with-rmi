package controller;

import dto.ItemDTO;
import dto.OrderDTO;
import dto.TableModel.ItemCategoryTM;
import dto.TableModel.ItemTM;
import javafx.collections.ObservableList;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;

public interface ItemController extends Remote {

    public ArrayList<ItemDTO> getItems() throws Exception;

    public String getItemID(String name) throws Exception;

    public boolean addItem(ItemDTO itemDTO)throws RemoteException;

    public boolean updateItem(ItemDTO itemDTO)throws RemoteException;

    public boolean deleteItem(String iid)throws RemoteException;

    public ItemDTO searchItem(String iid)throws RemoteException;

    public ItemDTO searchItemByName(String name)throws RemoteException;

    public double getItemPrice(String name) throws Exception;

    public ArrayList<ItemCategoryTM> getCategory() throws Exception;

    public ArrayList<ItemDTO> getAllItems() throws Exception;

    public ArrayList<ItemDTO> searchItemData(String text) throws Exception;

    String getItemName(String iid) throws Exception;

    ArrayList<ItemDTO> getItem(String iid) throws Exception;
}
