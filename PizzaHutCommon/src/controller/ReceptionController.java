package controller;

import dto.ReceptionDTO;
import dto.TableModel.ReceptionTM;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ReceptionController extends Remote {
    boolean add(ReceptionTM receptionTM) throws Exception;

    boolean deleteItem(String rid) throws Exception;

    ArrayList<ReceptionDTO> getAll() throws Exception;

    ArrayList<ReceptionDTO>searchByID(String id) throws Exception;

    ArrayList<ReceptionDTO>searchByName(String name) throws Exception;

    boolean Update(ReceptionDTO receptionDTO) throws Exception;
}
