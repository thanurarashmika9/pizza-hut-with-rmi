package controller;

import dto.TableModel.DeliveryTM;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface DeliveryController extends Remote {
    ArrayList<DeliveryTM> getAll() throws Exception;

    ArrayList <DeliveryTM> searchByID(String did) throws Exception;

    ArrayList<DeliveryTM> searchByName(String name) throws Exception;

    boolean update(DeliveryTM deliveryTM) throws Exception;

    boolean add(DeliveryTM deliveryTM) throws Exception;

    boolean delete(String id) throws Exception;
}
