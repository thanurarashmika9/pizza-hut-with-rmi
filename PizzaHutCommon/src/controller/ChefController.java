package controller;

import dto.CheffDTO;
import dto.LoginDTO;
import observer.ChefObserver;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface ChefController extends Remote {


    public ArrayList<CheffDTO>searchCheff(String text) throws Exception;

    ArrayList<CheffDTO> viewChef() throws Exception;

    boolean addChef(CheffDTO cheffDTO) throws Exception;

    boolean Update(CheffDTO cheffDTO) throws Exception;

    ArrayList<CheffDTO> searchByName(String name) throws Exception;

    boolean delete(String chid) throws Exception;

    public void addChefObserver(ChefObserver chefObserver)throws RemoteException;

    public void removeChefObserver(ChefObserver chefObserver)throws RemoteException;

    ArrayList<LoginDTO> login() throws Exception;

    int getCount()throws RemoteException;
}
