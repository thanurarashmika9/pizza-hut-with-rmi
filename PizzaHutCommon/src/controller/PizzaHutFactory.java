package controller;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PizzaHutFactory extends Remote {

    public CustomerController getCustomerController()throws RemoteException;

    public ItemController getItemController()throws RemoteException;

    public OrderController getOrderController()throws RemoteException;

    public LoginController getLoginController()throws RemoteException;

    public Order_StatsController getOrder_StatsController()throws RemoteException;

    public ChefController getChefController()throws RemoteException;

    DeliveryController getDeliveryController()throws RemoteException;

    ReceptionController getReceptionController()throws RemoteException;
}
