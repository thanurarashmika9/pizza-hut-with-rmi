package controller;

import dto.LoginDTO;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface LoginController extends Remote {

    public ArrayList<LoginDTO> getSecurityData() throws Exception;

    LoginDTO checkSecurity(String nameText) throws Exception;

    ArrayList<LoginDTO> check() throws Exception;

    ArrayList<LoginDTO> login() throws Exception;

    //ArrayList<LoginDTO> getAllLogingData()throws Exception;
}
