package controller;

import dto.CustomerDTO;
import observer.CustomerObserver;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface CustomerController extends Remote {

    public boolean addCustomer(CustomerDTO customerDTO) throws Exception;

    public boolean updateCustomer(CustomerDTO customerDTO)throws RemoteException;

    public boolean deleteCustomer(String id) throws Exception;

    public ArrayList<CustomerDTO> searchCustomer(String id) throws Exception;

    public ArrayList<CustomerDTO>getAllCustomers() throws Exception;

    public ArrayList<CustomerDTO> searchCustomerbyName(String name) throws Exception;

    public ArrayList<CustomerDTO> getDailyCustomers() throws Exception;

    public String getCID() throws Exception;

    public CustomerDTO searchcustomer(String id) throws Exception;

    public boolean reserveCustomer(String id)throws RemoteException;

    public boolean releaseCustomer(String id)throws RemoteException;

    public void addCustomerObserver(CustomerObserver customerObserver)throws RemoteException;

    public void removeCustomerObserver(CustomerObserver customerObserver)throws RemoteException;
}
