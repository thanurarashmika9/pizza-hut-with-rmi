package controller;

import dto.Order_StatsDTO;
import observer.Order_StatObserver;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Order_StatsController extends Remote {

    public ArrayList<Order_StatsDTO> getData() throws Exception;

    public boolean addData(Order_StatsDTO order_statsDTO) throws Exception;

    public void addCustomerObserver(Order_StatObserver order_statObserver)throws RemoteException;

    public void removeCustomerObserver(Order_StatObserver order_statObserver)throws RemoteException;

    boolean updateTbl(String state,String oid) throws Exception;
}
