package connector;

import controller.*;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ServerConnector {
    private static ServerConnector serverConnector;

    private PizzaHutFactory pizzaHutFactory;
    private CustomerController customerController;
    private ItemController itemController;
    private OrderController orderController;
    private LoginController loginController;
    private Order_StatsController order_statsController;
    private ChefController chefController;

    private ServerConnector() throws RemoteException, NotBoundException, MalformedURLException {
        pizzaHutFactory= (PizzaHutFactory) Naming.lookup("rmi://localhost:5050/PizzaServer");
    }
    public static ServerConnector getInstance() throws RemoteException, NotBoundException, MalformedURLException {
        if(serverConnector==null){
            serverConnector=new ServerConnector();
        }
        return serverConnector;
    }
    public CustomerController getCustomerController()throws RemoteException{
        if(customerController==null){
            customerController=pizzaHutFactory.getCustomerController();
        }
        return customerController;
    }
    public ItemController getItemController()throws RemoteException{
        if (itemController == null) {
            itemController=pizzaHutFactory.getItemController();
        }
        return itemController;
    }
    public OrderController getOrderController()throws RemoteException{
        if (orderController == null) {
            orderController=pizzaHutFactory.getOrderController();
        }
        return orderController;
    }
    public LoginController getLoginController()throws RemoteException{
        if (loginController == null) {
            loginController=pizzaHutFactory.getLoginController();
        }
        return loginController;
    }
    public Order_StatsController getOrder_statsController()throws RemoteException{
        if (order_statsController == null) {
            order_statsController=pizzaHutFactory.getOrder_StatsController();
        }
        return order_statsController;
    }

    public ChefController getChefController()throws RemoteException{
        if (chefController == null) {
            chefController=pizzaHutFactory.getChefController();
        }
        return chefController;
    }
}