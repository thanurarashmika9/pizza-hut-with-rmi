package main;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StartUp extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("../view/Main.fxml")));
        primaryStage.setScene(scene);
        primaryStage.show();

        /*primaryStage.setOnCloseRequest(event -> {
            if (MainController.getMainController()!=null){
                MainController.getMainController().remove();
            }
        });*/
    }
}
