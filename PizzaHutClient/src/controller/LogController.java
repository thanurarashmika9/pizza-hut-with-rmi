package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class LogController implements Initializable {

    @FXML
    private AnchorPane leftPane;

    @FXML
    private JFXTextField txtUName;

    @FXML
    private JFXPasswordField txtPW;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private JFXButton btnCancelLogin;

    @FXML
    void cancelLogin(ActionEvent event) {

    }

    @FXML
    void login(ActionEvent event) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
