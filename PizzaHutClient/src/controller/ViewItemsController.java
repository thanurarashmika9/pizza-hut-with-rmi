package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.ItemDTO;
import dto.TableModel.ItemCategoryTM;
import dto.TableModel.ItemTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewItemsController implements Initializable {

    @FXML
    public JFXButton btnReload;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private JFXTextField txtItemName;

    @FXML
    private TableView<ItemDTO> tblItems;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private TableView<ItemCategoryTM> tblCategory;

    @FXML
    void doSearch(ActionEvent event) {
        serachItems();
    }

    @FXML
    void searchItem(ActionEvent event) {
        serachItems();
    }

    private void serachItems(){
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            String item=txtItemName.getText().toUpperCase();
            if(item.startsWith("I") && item.matches(".*\\d+.*")) {
                ArrayList<ItemDTO> itemDTOS = itemController.searchItemData(item);
                ObservableList<ItemDTO> itemDTOS1 = FXCollections.observableArrayList();
                for (ItemDTO itemDTO : itemDTOS) {
                    itemDTOS1.add(new ItemDTO(itemDTO.getIid(), itemDTO.getDescription(), itemDTO.getUnit_price(), itemDTO.getCategory()));
                }
                tblItems.setItems(itemDTOS1);
            }else{
                ArrayList<ItemDTO> item1 = itemController.getItem(txtItemName.getText());
                ObservableList<ItemDTO>item2=FXCollections.observableArrayList();
                for (ItemDTO itemDTO : item1) {
                    item2.add(new ItemDTO(itemDTO.getIid(), itemDTO.getDescription(), itemDTO.getUnit_price(), itemDTO.getCategory()));
                }
                tblItems.setItems(item2);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTables(){
        try {
            ItemController itemController= ServerConnector.getInstance().getItemController();
            ArrayList<ItemCategoryTM> category = itemController.getCategory();
            ObservableList<ItemCategoryTM>itemCategoryTMS= FXCollections.observableArrayList();
            for(ItemCategoryTM itemCategoryTM : category){
                itemCategoryTMS.add(itemCategoryTM);
            }
            tblCategory.setItems(itemCategoryTMS);

            ArrayList<ItemDTO> items = itemController.getAllItems();
            ObservableList<ItemDTO>itemDTOS=FXCollections.observableArrayList();
            for(ItemDTO itemDTO : items){
                itemDTOS.add(itemDTO);
            }
            tblItems.setItems(itemDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblItems.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("iid"));
        tblItems.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("description"));
        tblItems.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("unit_price"));
        tblItems.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("category"));

        tblCategory.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("catID"));
        tblCategory.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("category"));

        loadTables();

    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Home.fxml"));
            if(!mainPane.getChildren().isEmpty()){
                mainPane.getChildren().clear();
                mainPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        loadTables();
    }

    @FXML
    public void selected(MouseEvent mouseEvent) {
    }
}
