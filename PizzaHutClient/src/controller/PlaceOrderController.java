package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.CustomerDTO;
import dto.ItemDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.OrderTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class PlaceOrderController implements Initializable {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private JFXTextField txtCusID;

    @FXML
    private JFXTextField txtCusName;

    @FXML
    private JFXComboBox<String> comboItem;

    @FXML
    private JFXTextField txtPrice;

    @FXML
    private TableView<OrderTM> tblOrder;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXButton btnCancelAdd;

    @FXML
    private JFXButton btnProceed;

    @FXML
    private JFXButton btnOrderCancel;

    @FXML
    private JFXTextField txtQuantity;

    @FXML
    private JFXTextField txtCusAddress;

    @FXML
    private JFXTextField txtCusContact;

    @FXML
    private Label lblOrderID;

    @FXML
    private JFXTextField txtGrandTotal;

    private void addToTable(){
        String cid=txtCusID.getText();
        String name=txtCusName.getText();
        String item = comboItem.getValue();
        int qty= Integer.parseInt(txtQuantity.getText());
        double total= Double.parseDouble(txtPrice.getText());
        double netTotal=total*qty;
        ObservableList<OrderTM> orderTMS= FXCollections.observableArrayList();
        orderTMS.add(new OrderTM(cid,name,item,qty,netTotal));
        tblOrder.setItems(orderTMS);
    }

    private void loadCombo(){
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            ObservableList<String>items=FXCollections.observableArrayList();
            ArrayList<ItemDTO>itemDTO=itemController.getItems();
            for (ItemDTO itemDTOs : itemDTO) {
                items.add(itemDTOs.getDescription());
            }
            comboItem.setItems(items);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPrice(){
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            double itemPrice = itemController.getItemPrice(comboItem.getValue());
            txtPrice.setText(String.valueOf(itemPrice));
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID(){
        try {
            /*OrderController orderController=ServerConnector.getInstance().getOrderController();
            String oid = orderController.getOID();
            String substringOID = oid.substring(1,oid.length());
            int intOid = Integer.parseInt(substringOID)+1;
            String fOID="O".concat(String.valueOf(intOid));
            lblOrderID.setText(fOID);*/
            lblOrderID.setText(IDGenerator.getNewID("Orders","oid","O"));

            /*CustomerController customerController=ServerConnector.getInstance().getCustomerController();
            String cid = customerController.getCID();
            int intCid = Integer.parseInt(cid.substring(1,cid.length()));
            String fCID="C".concat(String.valueOf(intCid+1));
            System.out.println(fCID);
            txtCusID.setText(fCID);*/
            txtCusID.setText(IDGenerator.getNewID("Customer","cid","C"));

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void AddtoTbl(ActionEvent event) {
        addToTable();
    }

    @FXML
    void cancelAddingTbl(ActionEvent event) {
        txtCusID.setText("");
        txtCusName.setText("");
        txtCusAddress.setText("");
        txtCusContact.setText("");
        txtQuantity.setText("");
        txtPrice.setText("");
    }

    @FXML
    void cancelOrder(ActionEvent event) {

    }

    @FXML
    void doAddTable(ActionEvent event) {
        addToTable();
    }

    @FXML
    void focusCusAddress(ActionEvent event) {
        txtCusAddress.requestFocus();
    }

    @FXML
    void focusCusContact(ActionEvent event) {
        txtCusContact.requestFocus();
    }

    @FXML
    void focusCusName(ActionEvent event) {
        txtCusName.requestFocus();
    }

    @FXML
    void focusItem(ActionEvent event) {
        comboItem.requestFocus();
    }

    @FXML
    void focusPrice(ActionEvent event) {
        txtPrice.requestFocus();
    }

    @FXML
    void focusQty(ActionEvent event) {
        txtQuantity.requestFocus();
        getPrice();
    }


    @FXML
    void proceedOrder(ActionEvent event) {
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            CustomerController customerController=ServerConnector.getInstance().getCustomerController();
            Order_StatsController order_statsController=ServerConnector.getInstance().getOrder_statsController();
            String iid=itemController.getItemID(comboItem.getValue());
            double price= Double.parseDouble(txtPrice.getText());
            int qty= Integer.parseInt(txtQuantity.getText());
            double total=price*qty;
            boolean b = customerController.addCustomer(new CustomerDTO(txtCusID.getText(), txtCusName.getText(), txtCusAddress.getText(), Integer.parseInt(txtCusContact.getText())));
            if (b) {
                OrderDTO orderDTO=new OrderDTO(lblOrderID.getText(), Date.valueOf(LocalDate.now()), txtCusID.getText(), iid, price, qty, "RE001", "Pending", "Pending", total);
                boolean b1 = orderController.makeOrder(orderDTO);
                if(b1){
                    order_statsController.addData(new Order_StatsDTO(lblOrderID.getText(),"Pending"));
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Order Placed");
                    alert.show();
                }else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Making Order Failed");
                    alert.show();
                }
            }else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Customer Saving Failed");
                alert.show();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        getID();
        txtCusName.setText("");
        txtCusAddress.setText("");
        txtCusContact.setText("");
        txtQuantity.setText("");
        txtPrice.setText("");
    }

    @FXML
    void selected(MouseEvent event) {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblOrder.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("cid"));
        tblOrder.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("CName"));
        tblOrder.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("item"));
        tblOrder.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("quantity"));
        tblOrder.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("total"));
        loadCombo();
        getID();
        txtCusName.requestFocus();
    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Home.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
