package controller;


import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.Order_StatsDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class StatusController implements Initializable {

    @FXML
    private AnchorPane leftPane;

    @FXML
    private TableView<Order_StatsDTO> tblOrderStat;

    @FXML
    private JFXTextField txtOrderID;

    @FXML
    private JFXButton btnCheck;

    @FXML
    void checkOrder(ActionEvent event) {
        checkAnOrder();
    }

    /*public String checkOrders(){
        String text = txtOrderID.getText();
        return text;
    }*/

    public void checkAnOrder(){
        /*MainController mainController=new MainController();

        AnchorPane rootPane = mainController.getController().rootPane;
        try {
            Node node=FXMLLoader.load(this.getClass().getResource("../view/ViewOrders.fxml"));

            if(!rootPane.getChildren().isEmpty()) {
                rootPane.getChildren().remove(0);
                rootPane.getChildren().add(node);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    @FXML
    void doCheck(ActionEvent event) {
        checkAnOrder();
    }

    @FXML
    void selected(MouseEvent event) {
        String oid = tblOrderStat.getSelectionModel().getSelectedItem().getOid();
        txtOrderID.setText(oid);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        StatusController statusController=this;
        tblOrderStat.getColumns().get(0).setStyle("fx-alignment : CENTER");
        tblOrderStat.getColumns().get(1).setStyle("fx-alignment : CENTER");
        tblOrderStat.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("oid"));
        tblOrderStat.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("status"));
        //loadtbl();
    }

    /*private void loadtbl() {
        try {
            Order_StatsController order_statsController= ServerConnector.getInstance().getOrder_statsController();
            ArrayList<Order_StatsDTO> data = order_statsController.getData();
            ObservableList<Order_StatsDTO>order_statsDTOS= FXCollections.observableArrayList();
            for (Order_StatsDTO order_statsDTO : data) {
                order_statsDTOS.add(new Order_StatsDTO(order_statsDTO.getOid(), order_statsDTO.getStatus()));
            }
            tblOrderStat.setItems(order_statsDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}
