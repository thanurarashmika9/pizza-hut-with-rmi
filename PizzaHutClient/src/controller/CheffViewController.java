package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class CheffViewController implements Initializable {

    @FXML
    private AnchorPane mainPane;

    @FXML
    private JFXTextField txtCusID;

    @FXML
    private TableView<?> tblCustomer;

    @FXML
    private JFXButton btnSearch;

    @FXML
    private JFXButton btnManage;

    @FXML
    void ManageChef(ActionEvent event) {

    }

    @FXML
    void doSearch(ActionEvent event) {

    }

    @FXML
    void searchCustomer(ActionEvent event) {
        try {
            ChefController chefController= ServerConnector.getInstance().getChefController();
            chefController.searchCheff(txtCusID.getText());
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Home.fxml"));
            if(!mainPane.getChildren().isEmpty()){
                mainPane.getChildren().clear();
                mainPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
