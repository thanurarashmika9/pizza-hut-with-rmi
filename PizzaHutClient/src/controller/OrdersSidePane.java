package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.OrderDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class OrdersSidePane implements Initializable {

    @FXML
    private AnchorPane leftPane;

    @FXML
    private TableView<OrderDTO> tblTodayOrders;

    @FXML
    private JFXTextField txtOrderID;

    @FXML
    private JFXButton btnCheck;

    @FXML
    void checkOrder(ActionEvent event) {

    }

    @FXML
    void doCheck(ActionEvent event) {

    }

    private void gettbl(){
        try {
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            ArrayList<OrderDTO> orders = orderController.orders(String.valueOf(LocalDate.now()));
            ObservableList<OrderDTO>orderDTOS= FXCollections.observableArrayList();
            for (OrderDTO orderDTO :
                    orders) {
                orderDTOS.add(new OrderDTO(orderDTO.getOid(),orderDTO.getCid()));
            }
            tblTodayOrders.setItems(orderDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblTodayOrders.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("oid"));
        tblTodayOrders.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("cid"));
        gettbl();
    }
}
