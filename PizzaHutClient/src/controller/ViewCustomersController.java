package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.CustomerDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewCustomersController implements Initializable {

    @FXML
    public JFXButton btnReload;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private JFXTextField txtCusID;

    @FXML
    private TableView<CustomerDTO> tblCustomer;

    @FXML
    private JFXButton btnSearch;

    @FXML
    void doSearch(ActionEvent event) {
        searchCustomer();
    }

    private void getAllCustomers(){
        try {
            CustomerController customerController=ServerConnector.getInstance().getCustomerController();
            ArrayList<CustomerDTO> allCustomers = customerController.getAllCustomers();
            ObservableList<CustomerDTO>customerDTOS=FXCollections.observableArrayList();
            for(CustomerDTO customerDTO : allCustomers){
                customerDTOS.add(customerDTO);
            }
            tblCustomer.setItems(customerDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ViewCustomersController controller;

    public ViewCustomersController getController(){
        return controller;
    }

    public void searchCustomer(){
        try {
            CustomerController customerController= ServerConnector.getInstance().getCustomerController();
            String cusID = txtCusID.getText().toUpperCase();
            if(cusID.startsWith("C") && cusID.matches(".*\\d+.*")) {
                ArrayList<CustomerDTO> customerDTOS = customerController.searchCustomer(cusID);
                ObservableList<CustomerDTO>strings=FXCollections.observableArrayList();
                for (CustomerDTO customerDTO1 : customerDTOS) {
                    strings.add(new CustomerDTO(customerDTO1.getCid(),customerDTO1.getName(),customerDTO1.getAddress(),customerDTO1.getContact()));
                }
                tblCustomer.setItems(strings);
            }else{
                ArrayList<CustomerDTO> customerDTOS = customerController.searchCustomerbyName(cusID);
                ObservableList<CustomerDTO>customerDTOS1= FXCollections.observableArrayList();
                for (CustomerDTO customerDTO : customerDTOS) {
                    customerDTOS1.add(customerDTO);
                }
                tblCustomer.setItems(customerDTOS1);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void searchCustomer(ActionEvent event) {
        searchCustomer();
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblCustomer.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("cid"));
        tblCustomer.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblCustomer.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblCustomer.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));
        getAllCustomers();
    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Home.fxml"));
            if(!mainPane.getChildren().isEmpty()){
                mainPane.getChildren().clear();
                mainPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        getAllCustomers();
    }
}
