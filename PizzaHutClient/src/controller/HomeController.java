package controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeController implements Initializable {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private JFXButton btnMakeOrder;

    @FXML
    private JFXButton btnViewOrders;

    @FXML
    private JFXButton btnViewItems;

    @FXML
    private JFXButton btnViewCustomers;

    @FXML
    private JFXButton btnViewCheff;

    @FXML
    private JFXButton btnLogOut;

    @FXML
    public void makeOrder(ActionEvent actionEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Place Order.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewOrders(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/ViewOrders.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewItems(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/View Items.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewCustomers(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/View Customers.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewCheff(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/CheffView.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void logOut(ActionEvent actionEvent) {

    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
