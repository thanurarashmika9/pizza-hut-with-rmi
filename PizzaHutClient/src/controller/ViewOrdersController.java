package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.OrderDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class ViewOrdersController implements Initializable {

    @FXML
    public Label lblDate;

    @FXML
    public JFXComboBox<String> comboSearchType;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private JFXTextField txtOrderID;

    @FXML
    private TableView<OrderDTO> tblOrders;

    @FXML
    private JFXButton btnSearch;

    @FXML
    void doSearch(ActionEvent event) {
        searchAction();
    }

    @FXML
    void search(ActionEvent event) {
        searchAction();
    }

    public void searchAction(){
        ObservableList<OrderDTO>orderDTOS1=FXCollections.observableArrayList();
        try {
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            String order = txtOrderID.getText().toUpperCase();
            if(order.startsWith("O")) {
                ArrayList<OrderDTO> orderDTOS = orderController.searchByOID(order);
                for (OrderDTO orderDTO : orderDTOS) {
                    orderDTOS1.add(new OrderDTO(orderDTO.getOid(),orderDTO.getoDate(),orderDTO.getCid(),orderDTO.getIid(),orderDTO.getPrice(),orderDTO.getQty(),orderDTO.getRid(),orderDTO.getChid(),orderDTO.getDid(),orderDTO.getTotal()));
                }
                tblOrders.setItems(orderDTOS1);
            }else if(order.startsWith("C")){
                ArrayList<OrderDTO> orderDTOS = orderController.searchByCID(order);
                for (OrderDTO orderDTO : orderDTOS) {
                    orderDTOS1.add(new OrderDTO(orderDTO.getOid(),orderDTO.getoDate(),orderDTO.getCid(),orderDTO.getIid(),orderDTO.getPrice(),orderDTO.getQty(),orderDTO.getRid(),orderDTO.getChid(),orderDTO.getDid(),orderDTO.getTotal()));
                }
                tblOrders.setItems(orderDTOS1);
            }else if (order.startsWith("I")){
                ArrayList<OrderDTO> orderDTOS = orderController.searchByIID(order);
                for (OrderDTO orderDTO : orderDTOS) {
                    orderDTOS1.add(new OrderDTO(orderDTO.getOid(),orderDTO.getoDate(),orderDTO.getCid(),orderDTO.getIid(),orderDTO.getPrice(),orderDTO.getQty(),orderDTO.getRid(),orderDTO.getChid(),orderDTO.getDid(),orderDTO.getTotal()));
                }
                tblOrders.setItems(orderDTOS1);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchOrders(){
        try {
            OrderController orderController= ServerConnector.getInstance().getOrderController();
                ArrayList<OrderDTO> orders = orderController.getOrders();
                ObservableList<OrderDTO> orderDTOS = FXCollections.observableArrayList();
                for (OrderDTO orderDTO : orders) {
                    orderDTOS.add(new OrderDTO(orderDTO.getOid(), orderDTO.getCid(), orderDTO.getIid(), orderDTO.getPrice(), orderDTO.getQty(), orderDTO.getRid(), orderDTO.getChid(), orderDTO.getDid(), orderDTO.getTotal()));
                }
                tblOrders.setItems(orderDTOS);
        } catch (RemoteException e1) {
            e1.printStackTrace();
        } catch (NotBoundException e1) {
            e1.printStackTrace();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private  void getTblCont(){

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblOrders.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("oid"));
        tblOrders.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("cid"));
        tblOrders.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("iid"));
        tblOrders.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("price"));
        tblOrders.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("qty"));
        tblOrders.getColumns().get(5).setCellValueFactory(new PropertyValueFactory<>("rid"));
        tblOrders.getColumns().get(6).setCellValueFactory(new PropertyValueFactory<>("chid"));
        tblOrders.getColumns().get(7).setCellValueFactory(new PropertyValueFactory<>("did"));
        tblOrders.getColumns().get(8).setCellValueFactory(new PropertyValueFactory<>("total"));

        ObservableList<String>strings=FXCollections.observableArrayList();
        strings.add(0,"Order");
        strings.add(1,"Customer");
        strings.add(2,"Item");

        searchOrders();
    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("../view/Home.fxml"));
            if(!mainPane.getChildren().isEmpty()){
                mainPane.getChildren().clear();
                mainPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void selected(MouseEvent mouseEvent) {
        txtOrderID.setText(tblOrders.getSelectionModel().getSelectedItem().getOid()+"/"+tblOrders.getSelectionModel().getSelectedItem().getCid()+"/"+tblOrders.getSelectionModel().getSelectedItem().getIid());
    }
}
