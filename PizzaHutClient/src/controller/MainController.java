package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.CustomerDTO;
import dto.LoginDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.OrderTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import observer.CustomerObserver;
import observer.Order_StatObserver;
import observerImpl.CustomerObserverImpl;
import observerImpl.Order_StatObserverImpl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    public Label lblReception;

    @FXML
    public Label lblDate;

    @FXML
    public Label lblOrderID;

    @FXML
    public JFXTextField txtCustID;

    @FXML
    public JFXButton btnCheck;

    @FXML
    public TableView<CustomerDTO> tblCustomers;

    @FXML
    public JFXButton btnMakeOrder;

    @FXML
    public JFXButton btnViewOrders;

    @FXML
    public JFXButton btnViewItems;

    @FXML
    public JFXButton btnViewCustomers;

    @FXML
    public JFXButton btnViewCheff;

    @FXML
    public JFXButton btnLogOut;

    @FXML
    public AnchorPane leftPane1;

    @FXML
    public TableView<Order_StatsDTO> tblOrderStat;

    @FXML
    public JFXTextField txtOrderID;

    @FXML
    public JFXButton btnCheck1;

    @FXML
    public AnchorPane leftMainPane;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private AnchorPane leftPane;

    @FXML
    private JFXTextField txtUName;

    @FXML
    private JFXPasswordField txtPW;

    @FXML
    private JFXButton btnLogin;

    @FXML
    private JFXButton btnCancelLogin;

    @FXML
    private AnchorPane rightPane;

    @FXML
    private Label lblIID;

    @FXML
    private Label lblIDescription;

    @FXML
    private Label lblIPrice;

    @FXML
    private Label lblICategory;

    @FXML
    private AnchorPane notPane;

    @FXML
    private AnchorPane notPane1;

    @FXML
    private AnchorPane notPane2;

    @FXML
    private AnchorPane notPane3;

    @FXML
    private AnchorPane notPane4;

    @FXML
    private AnchorPane notPane5;

    @FXML
    private AnchorPane notPane6;

    @FXML
    public AnchorPane rootPane;

    @FXML
    private JFXTextField txtCusID;

    @FXML
    private JFXTextField txtCusName;

    @FXML
    private JFXComboBox<?> comboItem;

    @FXML
    private JFXTextField txtPrice;

    @FXML
    private TableView<OrderTM> tblOrder;

    @FXML
    private JFXButton btnAdd;

    @FXML
    private JFXButton btnCancelAdd;

    @FXML
    private JFXButton btnProceed;

    @FXML
    private JFXButton btnOrderCancel;

    @FXML
    private JFXTextField txtGrandTotal;

    @FXML
    private JFXTextField txtQuantity;

    @FXML
    private JFXTextField txtCusAddress;

    @FXML
    private JFXTextField txtCusContact;



    public static MainController mainController;

    private CustomerObserver customerObserver;

    private Order_StatObserver order_statObserver;

    @FXML
    void cancelLogin(ActionEvent event) {
        txtUName.setText("");
        txtPW.setText("");
    }


    @FXML
    void login(ActionEvent event) throws Exception {
        logIn();
    }

    private void logIn(){
        try {
            LoginController loginController = ServerConnector.getInstance().getLoginController();
            ArrayList<LoginDTO> check = loginController.check();
            String un = null;
            String pw = null;
            for (LoginDTO loginDTO : check) {
                if (txtUName.getText().matches(loginDTO.getUsername()) && txtPW.getText().matches(loginDTO.getPassword())) {
                    un = loginDTO.getUsername();
                    pw = loginDTO.getPassword();
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Access Granted");
                    alert.show();
                    notPane.setEffect(null);
                    rootPane.setEffect(null);
                    rootPane.setDisable(false);
                    rightPane.setEffect(null);
                    rightPane.setDisable(false);

                    leftPane1.setVisible(true);
                    leftPane.setVisible(false);
                    lblReception.setText(txtUName.getText());
                    return;
                }else{
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Access Denied");
                    alert.show();
                }
            }
            System.out.println(un);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void proceedOrder(ActionEvent event) {

    }

    @FXML
    void selected(MouseEvent event) {
        String cid = tblCustomers.getSelectionModel().getSelectedItem().getCid();
        txtCustID.setText(cid);
    }

    private void loadCustomerTbl(){
        System.out.println("LoadCustomer");
        try {
            CustomerController customerController=ServerConnector.getInstance().getCustomerController();
            ArrayList<CustomerDTO> allCustomers = customerController.getDailyCustomers();
            ObservableList<CustomerDTO>customerDTOS= FXCollections.observableArrayList();
            for (CustomerDTO customerDTO : allCustomers) {
                customerDTOS.add(new CustomerDTO(customerDTO.getCid(),customerDTO.getName()));
            }
            tblCustomers.setItems(customerDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*private MainController controller;

    public MainController getController(){
        return controller;
    }
    public AnchorPane getMainPane(){
        AnchorPane rootPane1 = rootPane;
        return rootPane1;
    }*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        leftPane1.setVisible(false);
        MainController mainController=this;
        GaussianBlur blur=new GaussianBlur();
        notPane.setEffect(blur);
        rootPane.setEffect(blur);
        rootPane.setDisable(true);
        rightPane.setEffect(blur);
        rightPane.setDisable(true);
        leftMainPane.setEffect(null);

        lblDate.setText(String.valueOf(LocalDate.now()));



        try {
            order_statObserver=new Order_StatObserverImpl(this);
            Order_StatsController order_statsController=ServerConnector.getInstance().getOrder_statsController();
            order_statsController.addCustomerObserver(order_statObserver);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        tblCustomers.getColumns().get(0).setStyle("-fx-alignment : CENTER");
        tblCustomers.getColumns().get(1).setStyle("-fx-alignment : CENTER");
        tblCustomers.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("cid"));
        tblCustomers.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));

        tblOrderStat.getColumns().get(0).setStyle("-fx-alignment : CENTER");
        tblOrderStat.getColumns().get(1).setStyle("-fx-alignment : CENTER");
        tblOrderStat.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("oid"));
        tblOrderStat.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("status"));

        loadtbl();

        loadCustomerTbl();

    }

    private void loadtbl() {
        try {
            Order_StatsController order_statsController= ServerConnector.getInstance().getOrder_statsController();
            ArrayList<Order_StatsDTO> data = order_statsController.getData();
            ObservableList<Order_StatsDTO>order_statsDTOS= FXCollections.observableArrayList();
            for (Order_StatsDTO order_statsDTO : data) {
                order_statsDTOS.add(new Order_StatsDTO(order_statsDTO.getOid(), order_statsDTO.getStatus()));
            }
            tblOrderStat.setItems(order_statsDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void checkCustomer(ActionEvent actionEvent) {
        loadCusUI();
        id=txtCustID.getText();
    }

    public static String id=null;

    private void loadCusUI(){
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/View Customers.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void doCheck(ActionEvent actionEvent) {
        loadCusUI();
        id=txtCustID.getText();
    }

    @FXML
    public void makeOrder(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/Place Order.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewOrders(ActionEvent actionEvent) {
        Load();
    }

    public  void Load(){
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/ViewOrders.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewItems(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/View Items.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewCustomers(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/View Customers.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void viewCheff(ActionEvent actionEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("../view/CheffView.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void logOut(ActionEvent actionEvent) {
        
    }

    @FXML
    public void checkOrder(ActionEvent actionEvent) {

    }

    @FXML
    public void selectedStat(MouseEvent mouseEvent) {
        String oid = tblOrderStat.getSelectionModel().getSelectedItem().getOid();
        txtOrderID.setText(oid);
    }

    public void updateAll() {
        System.out.println("Controller");
        loadCustomerTbl();
        System.out.println("1 OK");
        loadtbl();
        System.out.println("2 OK");
    }


    public static MainController getMainController(){
        return mainController;
    }

    @FXML
    public void ocusPw(ActionEvent actionEvent) {
        txtPW.requestFocus();
    }

    @FXML
    public void doLogin(ActionEvent actionEvent) {
        logIn();
    }

    /*public void remove() {
        try {
            customerController.removeCustomerObserver(customerObserver);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }*/
}
