package controller;

import connector.ServerConnector;
import dto.ItemDTO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

public class ItemsController implements Initializable {

    @FXML
    private AnchorPane rightPane;

    @FXML
    private Label lblIID;

    @FXML
    private Label lblIDescription;

    @FXML
    private Label lblIPrice;

    @FXML
    private Label lblICategory;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            ItemController itemController= ServerConnector.getInstance().getItemController();
            ItemDTO items = itemController.searchItem("ddd");
            lblIID.setText(items.getIid());
            lblIDescription.setText(items.getDescription());
            lblICategory.setText(items.getCategory());
            lblIPrice.setText(String.valueOf(items.getUnit_price()));
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
