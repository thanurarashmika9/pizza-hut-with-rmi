package observerImpl;

import controller.MainController;
import dto.Order_StatsDTO;
import observer.Order_StatObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Order_StatObserverImpl extends UnicastRemoteObject implements Order_StatObserver {

    private MainController mainController;

    public Order_StatObserverImpl(MainController mainController) throws RemoteException {
        this.mainController=mainController;
    }

    @Override
    public void update(Order_StatsDTO order_statsDTO) throws RemoteException {
        mainController.updateAll();
    }
}
