package observerImpl;

import controller.MainController;
import dto.CustomerDTO;
import observer.CustomerObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CustomerObserverImpl extends UnicastRemoteObject implements CustomerObserver {

    private MainController mainController;

    public CustomerObserverImpl(MainController mainController) throws RemoteException {
        this.mainController=mainController;
    }

    @Override
    public void update(CustomerDTO customerDTO) throws RemoteException {

    }
}
