package observerImpl;

import controller.CheffViewController;
import dto.CheffDTO;
import observer.ChefObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ChefObserverImpl extends UnicastRemoteObject implements ChefObserver {

    private CheffViewController cheffViewController;

    public ChefObserverImpl(CheffViewController cheffViewController) throws RemoteException {
        this.cheffViewController=cheffViewController;
    }

    @Override
    public void update(CheffDTO cheffDTO) throws RemoteException {
    }
}
