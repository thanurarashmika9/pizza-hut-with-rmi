package connector;

import controller.*;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class ServerConnector {
    private static ServerConnector serverConnector;

    private OrderController orderController;
    private PizzaHutFactory pizzaHutFactory;
    private ItemController itemController;
    private Order_StatsController order_statsController;
    private ChefController chefController;

    private ServerConnector() throws RemoteException, NotBoundException, MalformedURLException {
        pizzaHutFactory= (PizzaHutFactory) Naming.lookup("rmi://localhost:5050/PizzaServer");
    }
    public static ServerConnector getInstance() throws RemoteException, NotBoundException, MalformedURLException {
        if(serverConnector==null){
            serverConnector=new ServerConnector();
        }
        return serverConnector;
    }
    public OrderController getOrderController()throws RemoteException{
        if (orderController == null) {
            orderController=pizzaHutFactory.getOrderController();
        }
        return orderController;
    }
    public ItemController getItemController()throws RemoteException{
        if(itemController == null){
            itemController=pizzaHutFactory.getItemController();
        }
        return itemController;
    }
    public Order_StatsController getOrder_statsController()throws RemoteException{
        if (order_statsController == null) {
            order_statsController=pizzaHutFactory.getOrder_StatsController();
        }
        return order_statsController;
    }
    public ChefController getChefController()throws RemoteException{
        if(chefController == null){
            chefController=pizzaHutFactory.getChefController();
        }
        return chefController;
    }
}