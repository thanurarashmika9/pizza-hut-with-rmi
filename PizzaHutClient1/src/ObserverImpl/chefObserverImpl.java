package ObserverImpl;

import controller.CheffController;
import dto.CheffDTO;
import observer.ChefObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class chefObserverImpl extends UnicastRemoteObject implements ChefObserver {

    private CheffController cheffController;

    public chefObserverImpl(CheffController cheffController) throws RemoteException {
        this.cheffController=cheffController;
    }

    @Override
    public void update(CheffDTO cheffDTO) throws RemoteException {
        cheffController.reload();
    }
}
