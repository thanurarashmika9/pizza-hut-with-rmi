package controller;

import ObserverImpl.chefObserverImpl;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.DeliveryDTO;
import dto.LoginDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.DeliveryInfoTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import observable.ChefObservable;
import observer.ChefObserver;
import observerImpl.ChefObserverImpl;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class CheffController implements Initializable {

    @FXML
    public Label lblOrderCount;

    @FXML
    public AnchorPane subPane1;

    @FXML
    public TableView<DeliveryInfoTM> tblDelStat;

    @FXML
    public JFXTextField txtOrderDelivery;

    @FXML
    public JFXButton btnMakeDelivery1;

    @FXML
    public JFXButton btnFinishOrder;

    @FXML
    public AnchorPane mainPane1;

    @FXML
    public JFXTextField txtUN;

    @FXML
    public JFXPasswordField txtPW;

    @FXML
    public JFXButton btnLeave;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private AnchorPane mainPane;

    @FXML
    private Label txtOrderID;

    @FXML
    private Label txtItemID;

    @FXML
    private Label txtItemName;

    @FXML
    private JFXButton btnFinish;

    @FXML
    private AnchorPane subPane;

    @FXML
    private TableView<DeliveryDTO> tblDelivery;

    @FXML
    private JFXTextField txtDid;

    @FXML
    private JFXButton btnMakeDelivery;

    @FXML
    private JFXButton btnGetOrder;

    @FXML
    void finishOrder(ActionEvent event) {
        btnMakeDelivery.setDisable(false);
        btnLeave.setDisable(false);
    }

    @FXML
    void getOrder(ActionEvent event) {
        try {
            OrderController orderController= ServerConnector.getInstance().getOrderController();
            ItemController itemController=ServerConnector.getInstance().getItemController();
            Order_StatsController order_statsController=ServerConnector.getInstance().getOrder_statsController();
            OrderDTO order = orderController.getOrder();
            if(order!=null) {
                txtOrderID.setText(order.getOid());
                txtItemID.setText(order.getIid());
                String itemName = itemController.getItemName(order.getIid());
                txtItemName.setText(itemName);
                btnGetOrder.setDisable(true);
                orderController.updateCheff(order.getOid(), "X001");
                order_statsController.updateTbl("Cooking",order.getOid());
                btnLeave.setDisable(true);
            }else{
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("No Orders in the Queue");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void makeDelivery(ActionEvent event) {
        selectDelivery();
    }

    @FXML
    void selected(MouseEvent event) {

        txtDid.setText(tblDelivery.getSelectionModel().getSelectedItem().getName());
    }

    public void loadDeliveryInfo(){
        try {
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            ArrayList<DeliveryDTO> deliveryBoys = orderController.getDeliveryBoys();
            ObservableList<DeliveryDTO>deliveryDTOS= FXCollections.observableArrayList();
            for (DeliveryDTO deliveryDTO : deliveryBoys) {
                deliveryDTOS.add(new DeliveryDTO(deliveryDTO.getName()));
            }
            tblDelivery.setItems(deliveryDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getOrderCount(){
        try {
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            int orderCount = orderController.getOrderCount();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private ChefObserver chefObserver;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            chefObserver=new chefObserverImpl(this);
            ChefController chefController=ServerConnector.getInstance().getChefController();
            chefController.addChefObserver(chefObserver);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        tblDelivery.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblDelivery.getColumns().get(0).setStyle("-fx-alignment : CENTER");

        tblDelStat.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("oid"));
        tblDelStat.getColumns().get(0).setStyle("-fx-alignment : CENTER");
        tblDelStat.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("Delivery"));
        tblDelStat.getColumns().get(1).setStyle("-fx-alignment : CENTER");
        //getOrderCount();
        loadDeliveryInfo();
        loadInfoTable();
    }

    private void loadInfoTable(){
        try {
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            ArrayList<DeliveryInfoTM> deliveryStats = orderController.getDeliveryStats();
            ObservableList<DeliveryInfoTM>order_statsDTOS=FXCollections.observableArrayList();
            for(DeliveryInfoTM order_statsDTO : deliveryStats){
                order_statsDTOS.add(new DeliveryInfoTM(order_statsDTO.getOid(),order_statsDTO.getDelivery()));
            }
            tblDelStat.setItems(order_statsDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void doMakeDelivery(ActionEvent actionEvent) {
        selectDelivery();
    }

    private String test;

    @FXML
    public void selected1(MouseEvent mouseEvent) {
        test=tblDelStat.getSelectionModel().getSelectedItem().getOid();
    }

    @FXML
    public void doCheck(ActionEvent actionEvent) {

    }

    @FXML
    public void FinishDelivery(ActionEvent actionEvent) {
        if(test==null){
            Alert alert=new Alert(Alert.AlertType.WARNING);
            alert.setContentText("Please Select an Order to Proceed");
            alert.show();
        }else {
            try {
                String oid = tblDelStat.getSelectionModel().getSelectedItem().getOid();
                OrderController orderController=ServerConnector.getInstance().getOrderController();
                String delivery="Finished";
                finishDelivery(oid, delivery);
                loadInfoTable();
            } catch (RemoteException e) {
                e.printStackTrace();
            } catch (NotBoundException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void finishDelivery(String oid,String delivery){
        try {
            Order_StatsController order_statsController=ServerConnector.getInstance().getOrder_statsController();
            order_statsController.updateTbl("Finished",oid);
            loadDeliveryInfo();
            loadInfoTable();
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void selectDelivery(){
        try {
            String did = txtDid.getText();
            String oid = txtOrderID.getText();
            OrderController orderController=ServerConnector.getInstance().getOrderController();
            Order_StatsController order_statsController=ServerConnector.getInstance().getOrder_statsController();
            orderController.updateOrder(oid, did);
            boolean b = order_statsController.updateTbl("Delivering", oid);
            if(b){
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Sent to Delivery");
                alert.show();
                loadInfoTable();
                loadDeliveryInfo();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void leave(ActionEvent actionEvent) {
        try {
            ChefController chefController=ServerConnector.getInstance().getChefController();
            int count = chefController.getCount();
            if(count==1){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Can't Leave at the Moment");
                alert.show();
            }else if(count>=2){
                subPane.setVisible(false);
                subPane1.setVisible(false);
                mainPane.setVisible(false);
                mainPane1.setVisible(true);
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Leave Marked");
                alert.show();
                chefController.removeChefObserver(chefObserver);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void reload() {

    }

    @FXML
    public void focusPW(ActionEvent actionEvent) {
        txtPW.requestFocus();
    }

    @FXML
    public void login(ActionEvent actionEvent) {
        ChefController chefController= null;
        try {
            chefController = ServerConnector.getInstance().getChefController();
            ArrayList<LoginDTO> login = chefController.login();
            String id=null;
            String name=null;
            for(LoginDTO loginDTO : login){
                id=loginDTO.getPassword();
                name=loginDTO.getUsername();
            }

            if(txtUN.getText().equals(name) && txtPW.getText().equals(id)){
                System.out.println(name);
                System.out.println(id);
                mainPane1.setVisible(false);
                mainPane.setVisible(true);
                subPane1.setVisible(true);
                subPane.setVisible(true);
                btnGetOrder.setVisible(true);
            }else{
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Incorrect Input Data");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
