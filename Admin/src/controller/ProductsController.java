package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import db.DBConnection;
import dto.ItemDTO;
import dto.TableModel.ItemCategoryTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductsController implements Initializable {

    @FXML
    public JFXButton btnReload;

    @FXML
    private JFXButton btnPrint;

    @FXML
    private JFXButton btnNew;

    @FXML
    private JFXButton btnUpdate;

    @FXML
    private JFXButton btnRemove;

    @FXML
    private JFXTextField txtIID;

    @FXML
    private JFXTextField txtDescription;

    @FXML
    private JFXTextField txtPrice;

    @FXML
    private JFXTextField txtCategory;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    private TableView<ItemDTO> tblItems;

    @FXML
    private TableView<ItemCategoryTM> tblCategory;

    @FXML
    void add(ActionEvent event) {
        try {
            ItemController itemController= ServerConnector.getInstance().getItemController();
            if(txtIID.getText().isEmpty() || txtDescription.getText().isEmpty()  || txtPrice.getText().isEmpty() || txtCategory.getText().isEmpty()){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Please Provide All Required Data");
                alert.show();
            }else {
                /*String id = IDGenerator.getNewID("Item", "iid", "I");*/
                boolean b = itemController.addItem(new ItemDTO(txtIID.getText(), txtDescription.getText(), Double.parseDouble(txtPrice.getText()), txtCategory.getText()));
                if(b){
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("New Item Added Successfully");
                    alert.show();
                    loadTbl();
                }else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Failure in Adding New Item");
                    alert.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void focusCategory(ActionEvent event) {
        txtCategory.requestFocus();
    }

    @FXML
    void focusDesc(ActionEvent event) {
        txtDescription.requestFocus();
    }

    @FXML
    void focusPrice(ActionEvent event) {
        txtPrice.requestFocus();
    }

    @FXML
    void print(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/ItemView.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void remove(ActionEvent event) {
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Are You Sure to Delete");
            alert.setResizable(false);
            alert.setContentText("Select okay or cancel this alert.");
            alert.showAndWait();

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                boolean delete = itemController.deleteItem(tblItems.getSelectionModel().getSelectedItem().getIid());
                if(delete){
                    Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                    alert1.setContentText("Item Removed Successfully");
                    alert1.show();
                    loadTbl();
                }else{
                    Alert alert1=new Alert(Alert.AlertType.ERROR);
                    alert1.setContentText("Removing Failed");
                    alert1.show();
                }
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void search(ActionEvent event) {
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            String item=txtSearch.getText().toUpperCase();
            if(item.startsWith("I") && item.matches(".*\\d+.*")) {
                ArrayList<ItemDTO> itemDTOS = itemController.searchItemData(item);
                ObservableList<ItemDTO> itemDTOS1 = FXCollections.observableArrayList();
                for (ItemDTO itemDTO : itemDTOS) {
                    itemDTOS1.add(new ItemDTO(itemDTO.getIid(), itemDTO.getDescription(), itemDTO.getUnit_price(), itemDTO.getCategory()));
                }
                tblItems.setItems(itemDTOS1);
            }else{
                ArrayList<ItemDTO> item1 = itemController.getItem(txtDescription.getText());
                ObservableList<ItemDTO>item2=FXCollections.observableArrayList();
                for (ItemDTO itemDTO : item1) {
                    item2.add(new ItemDTO(itemDTO.getIid(), itemDTO.getDescription(), itemDTO.getUnit_price(), itemDTO.getCategory()));
                }
                tblItems.setItems(item2);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {
        txtIID.setText(tblItems.getSelectionModel().getSelectedItem().getIid());
        txtDescription.setText(tblItems.getSelectionModel().getSelectedItem().getDescription());
        txtPrice.setText(String.valueOf(tblItems.getSelectionModel().getSelectedItem().getUnit_price()));
        txtCategory.setText(tblItems.getSelectionModel().getSelectedItem().getCategory());
    }

    @FXML
    void update(ActionEvent event) {
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            boolean b = itemController.updateItem(new ItemDTO(tblItems.getSelectionModel().getSelectedItem().getIid(), txtDescription.getText(), Double.parseDouble(txtPrice.getText()), txtCategory.getText()));
            if(b){
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Item Updated Successfully");
                alert.show();
                loadTbl();
            }else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Failed Updating Item");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    private void loadTbl(){
        try {
            ItemController itemController=ServerConnector.getInstance().getItemController();
            ArrayList<ItemDTO> allItems = itemController.getAllItems();
            ObservableList<ItemDTO>itemDTOS= FXCollections.observableArrayList();
            for (ItemDTO itemDTO : allItems) {
                itemDTOS.add(new ItemDTO(itemDTO.getIid(), itemDTO.getDescription(), itemDTO.getUnit_price(), itemDTO.getCategory()));
            }
            tblItems.setItems(itemDTOS);
            ArrayList<ItemCategoryTM> category = itemController.getCategory();
            ObservableList<ItemCategoryTM>itemCategoryTMS=FXCollections.observableArrayList();
            for (ItemCategoryTM itemCategoryTM : category) {
                itemCategoryTMS.add(new ItemCategoryTM(itemCategoryTM.getCatID(), itemCategoryTM.getCategory()));
            }
            tblCategory.setItems(itemCategoryTMS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblItems.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("iid"));
        tblItems.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("description"));
        tblItems.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("unit_price"));
        tblItems.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("category"));

        tblCategory.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("catID"));
        tblCategory.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("category"));

        loadTbl();
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        loadTbl();
    }
}
