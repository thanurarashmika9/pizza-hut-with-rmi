package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import dto.LoginDTO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class adminController implements Initializable {

    @FXML
    public AnchorPane mainPane;

    @FXML
    public AnchorPane loginPane;

    @FXML
    public JFXTextField txtUN;

    @FXML
    public JFXPasswordField txtPw;

    @FXML
    private AnchorPane rootPane;

    @FXML
    private JFXButton btnCustomer;

    @FXML
    private JFXButton btnReception;

    @FXML
    private JFXButton btnChef;

    @FXML
    private JFXButton btnDelivery;

    @FXML
    private JFXButton btnReports;

    @FXML
    private JFXButton btnProducts;

    @FXML
    void viewChef(ActionEvent event) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("/view/chefManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewCustomer(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/customerView.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewDelivery(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/DeliveryManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewProducts(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/Products.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewReception(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/ReceptionManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewReports(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/Reports.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void goHome(MouseEvent mouseEvent) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/Home.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void Login(){
        try {
            LoginController loginController= ServerConnector.getInstance().getLoginController();
            ArrayList<LoginDTO> login = loginController.login();
            String un=null;
            String pw=null;
            for (LoginDTO loginDTO : login) {
                if(txtUN.getText().matches(loginDTO.getUsername()) && txtPw.getText().matches(loginDTO.getPassword())){
                    rootPane.setVisible(true);
                    loginPane.setVisible(false);
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Access Granted");
                    alert.show();
                }else{
                    Alert alert=new Alert(Alert.AlertType.WARNING);
                    alert.setContentText("Access Denied");
                    alert.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void focusPw(ActionEvent actionEvent) {
        txtPw.requestFocus();
    }

    @FXML
    public void doLogin(ActionEvent actionEvent) {
        Login();
    }
}
