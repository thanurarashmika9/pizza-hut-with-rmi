package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import db.DBConnection;
import dto.CheffDTO;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

public class ChefManagementController implements Initializable {

    @FXML
    public Label lblCid;

    @FXML
    public JFXButton btnReload;

    @FXML
    private TableView<CheffDTO> tblChef;

    @FXML
    private JFXButton btnPrint;

    @FXML
    private JFXButton btnNew;

    @FXML
    private JFXButton btnUpdate;

    @FXML
    private JFXButton btnRemove;

    @FXML
    private JFXTextField txtName;

    @FXML
    private JFXTextField txtAddress;

    @FXML
    private JFXTextField txtContact;

    @FXML
    private JFXTextField txtSalary;

    @FXML
    private JFXTextField txtSearch;

    private void getID(){
        try {
            lblCid.setText(IDGenerator.getNewID("Cheff","chid","X"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void add(ActionEvent event) {
        try {
            ChefController chefController=ServerConnector.getInstance().getChefController();
            if(txtName.getText().isEmpty() || txtAddress.getText().isEmpty()  || txtContact.getText().isEmpty() || txtSalary.getText().isEmpty()){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Please Provide All Required Data");
                alert.show();
            }else{
                boolean b = chefController.addChef(new CheffDTO(lblCid.getText(), txtName.getText(), txtAddress.getText(), Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
                getID();
                if(b){
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("New Chef Added Successfully");
                    alert.show();
                    loadTable();
                }else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Failure in Adding Chef");
                    alert.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void focusAddress(ActionEvent event) {
        txtAddress.requestFocus();
    }

    @FXML
    void focusContact(ActionEvent event) {
        txtContact.requestFocus();
    }

    @FXML
    void focusSalary(ActionEvent event) {
        txtSalary.requestFocus();
    }

    @FXML
    void print(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/viewCheff.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void remove(ActionEvent event) {
        try {
            ChefController chefController=ServerConnector.getInstance().getChefController();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Are You Sure to Delete");
            alert.setResizable(false);
            alert.setContentText("Select okay or cancel this alert.");
            alert.showAndWait();

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                boolean delete = chefController.delete(tblChef.getSelectionModel().getSelectedItem().getChid());
                if(delete){
                    Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                    alert1.setContentText("Cheff Removed Successfully");
                    alert1.show();
                    loadTable();
                }else{
                    Alert alert1=new Alert(Alert.AlertType.ERROR);
                    alert1.setContentText("Cheff Removing Failed");
                    alert1.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void search(ActionEvent event) {
        try {
            ChefController chefController=ServerConnector.getInstance().getChefController();
            ObservableList<CheffDTO>cheff=FXCollections.observableArrayList();
            String search = txtSearch.getText().toUpperCase();
            if(search.startsWith("X") && search.matches(".*\\d+.*")){
                ArrayList<CheffDTO> cheffDTOS = chefController.searchCheff(search);
                for (CheffDTO cheffDTO : cheffDTOS) {
                    cheff.add(new CheffDTO(cheffDTO.getChid(), cheffDTO.getName(), cheffDTO.getAddress(), cheffDTO.getContact(), cheffDTO.getSalary()));
                }
                tblChef.setItems(cheff);
            }else{
                ArrayList<CheffDTO> cheffDTOS = chefController.searchByName(txtSearch.getText());
                for (CheffDTO cheffDTO : cheffDTOS) {
                    cheff.add(new CheffDTO(cheffDTO.getChid(), cheffDTO.getName(), cheffDTO.getAddress(), cheffDTO.getContact(), cheffDTO.getSalary()));
                }
                tblChef.setItems(cheff);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {
        lblCid.setText(tblChef.getSelectionModel().getSelectedItem().getChid());
        txtName.setText(tblChef.getSelectionModel().getSelectedItem().getName());
        txtAddress.setText(tblChef.getSelectionModel().getSelectedItem().getAddress());
        txtContact.setText(String.valueOf(tblChef.getSelectionModel().getSelectedItem().getContact()));
        txtSalary.setText(String.valueOf(tblChef.getSelectionModel().getSelectedItem().getSalary()));
        //btnNew.setDisable(true);
    }

    @FXML
    void update(ActionEvent event) {
        String id=tblChef.getSelectionModel().getSelectedItem().getChid();
        try {
            ChefController chefController=ServerConnector.getInstance().getChefController();
            boolean update = chefController.Update(new CheffDTO(id, txtName.getText(), txtAddress.getText(), Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
            if(update){
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Chef Updated Successfully");
                alert.show();
                loadTable();
            }else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Chef Updating Failed");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTable(){
        try {
            ChefController chefController= ServerConnector.getInstance().getChefController();
            ArrayList<CheffDTO> cheffDTOS = chefController.viewChef();
            ObservableList<CheffDTO>cheffDTOS1= FXCollections.observableArrayList();
            for (CheffDTO cheffDTO : cheffDTOS) {
                cheffDTOS1.add(new CheffDTO(cheffDTO.getChid(), cheffDTO.getName(), cheffDTO.getAddress(), cheffDTO.getContact(), cheffDTO.getSalary()));
            }
            tblChef.setItems(cheffDTOS1);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblChef.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("chid"));
        tblChef.getColumns().get(0).setStyle("-fx-alignment : CENTER");
        tblChef.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblChef.getColumns().get(1).setStyle("-fx-alignment : CENTER");
        tblChef.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblChef.getColumns().get(2).setStyle("-fx-alignment : CENTER");
        tblChef.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));
        tblChef.getColumns().get(3).setStyle("-fx-alignment : CENTER");
        tblChef.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("salary"));
        tblChef.getColumns().get(4).setStyle("-fx-alignment : CENTER");

        loadTable();
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        loadTable();
    }
}
