package controller;

import com.jfoenix.controls.JFXButton;
import connector.ServerConnector;
import db.DBConnection;
import dto.CustomerDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import observer.CustomerObserver;
import observerImpl.CustomerObserverImpl;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class CustomerViewController implements Initializable {

    private CustomerObserver customerObserver;

    @FXML
    private TableView<CustomerDTO> tblCustomer;

    @FXML
    private JFXButton btnPrint;

    @FXML
    void print(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/CustomerReport.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {

    }

    private void loadCustomers(){
        try {
            CustomerController customerController= ServerConnector.getInstance().getCustomerController();
            ArrayList<CustomerDTO> allCustomers = customerController.getAllCustomers();
            ObservableList<CustomerDTO>customerDTOS= FXCollections.observableArrayList();
            for (CustomerDTO customerDTO : allCustomers) {
                customerDTOS.add(new CustomerDTO(customerDTO.getCid(), customerDTO.getName(), customerDTO.getAddress(), customerDTO.getContact()));
            }
            tblCustomer.setItems(customerDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            customerObserver=new CustomerObserverImpl(this);
            CustomerController customerController=ServerConnector.getInstance().getCustomerController();
            customerController.addCustomerObserver(customerObserver);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        tblCustomer.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("cid"));
        tblCustomer.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblCustomer.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblCustomer.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));

        loadCustomers();
    }

    public void reload() {
        loadCustomers();
    }
}
