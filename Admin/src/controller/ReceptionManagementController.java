package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import db.DBConnection;
import dto.ReceptionDTO;
import dto.TableModel.ReceptionTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

public class ReceptionManagementController implements Initializable {

    @FXML
    private TableView<ReceptionDTO> tblReception;

    @FXML
    private JFXButton btnPrint;

    @FXML
    private JFXButton btnNewReception;

    @FXML
    private JFXButton brnUpdateReception;

    @FXML
    private JFXButton btnRemoveReception;

    @FXML
    private JFXTextField txtName;

    @FXML
    private JFXTextField txtAddress;

    @FXML
    private JFXTextField txtContact;

    @FXML
    private JFXTextField txtSalary;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    void addReception(ActionEvent event) {
        try {
            ReceptionController receptionController= ServerConnector.getInstance().getReceptionController();
            if(txtName.getText().isEmpty() || txtAddress.getText().isEmpty()  || txtContact.getText().isEmpty() || txtSalary.getText().isEmpty()){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Please Provide All Required Data");
                alert.show();
            }else {
                String id = IDGenerator.getNewID("Reception", "rid", "R");
                boolean b = receptionController.add(new ReceptionTM(id, txtName.getText(), txtAddress.getText(),Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
                if(b){
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("New Receptionist Added Successfully");
                    alert.show();
                    loadTable();

                }else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Failure in Adding New Receptionist");
                    alert.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void focusAddress(ActionEvent event) {
        txtAddress.requestFocus();
    }

    @FXML
    void focusContact(ActionEvent event) {
        txtContact.requestFocus();
    }

    @FXML
    void focusSalary(ActionEvent event) {
        txtSalary.requestFocus();
    }

    @FXML
    void print(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/viewReception.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void removeReception(ActionEvent event) {
        try {
            ReceptionController receptionController=ServerConnector.getInstance().getReceptionController();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Are You Sure to Delete");
            alert.setResizable(false);
            alert.setContentText("Select okay or cancel this alert.");
            alert.showAndWait();

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                boolean delete = receptionController.deleteItem(tblReception.getSelectionModel().getSelectedItem().getRid());
                if(delete){
                    Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                    alert1.setContentText("Receptionist Removed Successfully");
                    alert1.show();
                    loadTable();
                }else{
                    Alert alert1=new Alert(Alert.AlertType.ERROR);
                    alert1.setContentText("Removing Failed");
                    alert1.show();
                }
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void search(ActionEvent event) {
        try {
            ReceptionController receptionController=ServerConnector.getInstance().getReceptionController();
            ObservableList<ReceptionDTO> reception= FXCollections.observableArrayList();
            String search = txtSearch.getText().toUpperCase();
            if(search.startsWith("RE") && search.matches(".*\\d+.*")){
                ArrayList<ReceptionDTO> receptionDTOS = receptionController.searchByID(search);
                for (ReceptionDTO receptionDTO : receptionDTOS) {
                    reception.add(new ReceptionDTO(receptionDTO.getRid(), receptionDTO.getName(), receptionDTO.getAddress(), receptionDTO.getContact(), receptionDTO.getSalary()));
                }
                tblReception.setItems(reception);
            }else{
                ArrayList<ReceptionDTO> receptionDTOS = receptionController.searchByName(txtSearch.getText());
                for (ReceptionDTO receptionDTO : receptionDTOS) {
                    reception.add(new ReceptionDTO(receptionDTO.getRid(), receptionDTO.getName(), receptionDTO.getAddress(), receptionDTO.getContact(), receptionDTO.getSalary()));
                }
                tblReception.setItems(reception);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {
        txtName.setText(tblReception.getSelectionModel().getSelectedItem().getName());
        txtAddress.setText(tblReception.getSelectionModel().getSelectedItem().getAddress());
        txtContact.setText(String.valueOf(tblReception.getSelectionModel().getSelectedItem().getContact()));
        txtSalary.setText(String.valueOf(tblReception.getSelectionModel().getSelectedItem().getSalary()));
    }

    @FXML
    void updateReception(ActionEvent event) {
        try {
            ReceptionController receptionController=ServerConnector.getInstance().getReceptionController();
            boolean update = receptionController.Update(new ReceptionDTO(tblReception.getSelectionModel().getSelectedItem().getRid(), txtName.getText(), txtAddress.getText(), Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
            if (update) {
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Receptionist Updated Successfully");
                alert.show();
                loadTable();
            }else{
                Alert alert=new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Receptionist Updating Failed");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTable(){
        try {
            ReceptionController receptionController=ServerConnector.getInstance().getReceptionController();
            ArrayList<ReceptionDTO> all = receptionController.getAll();
            ObservableList<ReceptionDTO>receptionDTOS=FXCollections.observableArrayList();
            for (ReceptionDTO receptionDTO : all) {
                receptionDTOS.add(new ReceptionDTO(receptionDTO.getRid(), receptionDTO.getName(), receptionDTO.getAddress(), receptionDTO.getContact(), receptionDTO.getSalary()));
            }
            tblReception.setItems(receptionDTOS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblReception.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("rid"));
        tblReception.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblReception.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblReception.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));
        tblReception.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("salary"));

        loadTable();
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        loadTable();
    }
}
