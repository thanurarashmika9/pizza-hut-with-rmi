package controller;

import java.sql.SQLException;

public class IDGenerator {
    public static String getNewID(String tblName, String colName, String prefix) throws Exception {
        String lastId = util.IDController.getLastID(tblName, colName);
        String newId;
        if (lastId != null) {
            String Id = "";
            char[] reg = lastId.toCharArray();
            for (int i = 1; i < reg.length; i++) {
                Id += reg[i];
            }
            int r = Integer.parseInt(Id);
            if (r < 9) {
                newId = prefix + "00" + (r + 1);
            } else if (r < 99) {
                newId = prefix + "0" + (r + 1);
            } else
                newId = prefix + (r + 1);
        } else {
            newId = prefix + "001";
        }
        return newId;
    }

//        IDGenerator.getNewID("tblname","coloumn Name", "C");

    public static String getNewID(String lastId, String prefix) throws SQLException, ClassNotFoundException {
        String Id = "";
        char[] reg = lastId.toCharArray();
        for (int i = 1; i < reg.length; i++) {
            Id += reg[i];
        }
        int r = Integer.parseInt(Id);
        if (r < 9) {
            return prefix + "00" + (r + 1);
        } else if (r < 99) {
            return prefix + "0" + (r + 1);
        }
        return prefix + (r + 1);
    }
}
