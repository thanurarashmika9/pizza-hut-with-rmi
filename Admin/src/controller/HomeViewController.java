package controller;

import com.jfoenix.controls.JFXButton;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomeViewController implements Initializable {

    @FXML
    private AnchorPane rootPane;

    @FXML
    private JFXButton btnCustomer;

    @FXML
    private JFXButton btnReception;

    @FXML
    private JFXButton btnChef;

    @FXML
    private JFXButton btnDelivery;

    @FXML
    private JFXButton btnReports;

    @FXML
    private JFXButton btnProducts;

    @FXML
    void viewChef(ActionEvent event) {
        try {
            AnchorPane pane= FXMLLoader.load(this.getClass().getResource("/view/chefManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewCustomer(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/customerView.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewDelivery(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/DeliveryManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewProducts(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/Products.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewReception(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/ReceptionManagement.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void viewReports(ActionEvent event) {
        try {
            AnchorPane pane=FXMLLoader.load(this.getClass().getResource("/view/Reports.fxml"));
            if(!rootPane.getChildren().isEmpty()){
                rootPane.getChildren().clear();
                rootPane.getChildren().add(pane);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
