package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import connector.ServerConnector;
import db.DBConnection;
import dto.TableModel.DeliveryTM;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.ResourceBundle;

public class DeliveryManagementController implements Initializable {

    @FXML
    private TableView<DeliveryTM> tblDelivery;

    @FXML
    private JFXButton btnPrint;

    @FXML
    private JFXButton btnNew;

    @FXML
    private JFXButton btnUpdate;

    @FXML
    private JFXButton btnRemove;

    @FXML
    private JFXTextField txtName;

    @FXML
    private JFXTextField txtAddress;

    @FXML
    private JFXTextField txtContact;

    @FXML
    private JFXTextField txtSalary;

    @FXML
    private JFXTextField txtSearch;

    @FXML
    void add(ActionEvent event) {
        try {
            DeliveryController deliveryController=ServerConnector.getInstance().getDeliveryController();
            if(txtName.getText().isEmpty() || txtAddress.getText().isEmpty()  || txtContact.getText().isEmpty() || txtSalary.getText().isEmpty()){
                Alert alert=new Alert(Alert.AlertType.WARNING);
                alert.setContentText("Please Provide All Required Data");
                alert.show();
            }else {
                String id = IDGenerator.getNewID("Deliver", "did", "T");
                boolean b = deliveryController.add(new DeliveryTM(id, txtName.getText(), txtAddress.getText(), Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
                if(b){
                    Alert alert=new Alert(Alert.AlertType.INFORMATION);
                    alert.setContentText("Added Successfully");
                    alert.show();
                    loadTable();
                }else{
                    Alert alert=new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Failure in Adding Deliverer");
                    alert.show();
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void focusAddress(ActionEvent event) {
        txtAddress.requestFocus();
    }

    @FXML
    void focusContact(ActionEvent event) {
        txtContact.requestFocus();
    }

    @FXML
    void focusSalary(ActionEvent event) {
        txtSalary.requestFocus();
    }

    @FXML
    void print(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/viewDeliverer.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void remove(ActionEvent event) {
        try {
            DeliveryController deliveryController=ServerConnector.getInstance().getDeliveryController();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation");
            alert.setHeaderText("Are You Sure to Delete");
            alert.setResizable(false);
            alert.setContentText("Select okay or cancel this alert.");
            alert.showAndWait();

            Optional<ButtonType> result = alert.showAndWait();
            if(result.get() == ButtonType.OK){
                boolean delete = deliveryController.delete(tblDelivery.getSelectionModel().getSelectedItem().getId());
                if(delete){
                    Alert alert1=new Alert(Alert.AlertType.INFORMATION);
                    alert1.setContentText("Deliverer Removed Successfully");
                    alert1.show();
                    loadTable();
                }else{
                    Alert alert1=new Alert(Alert.AlertType.ERROR);
                    alert1.setContentText("Removing Failed");
                    alert1.show();
                }
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void search(ActionEvent event) {
        try {
            DeliveryController deliveryController=ServerConnector.getInstance().getDeliveryController();
            String search = txtSearch.getText().toUpperCase();
            if(search.startsWith("T") && search.matches(".*\\d+.*")) {
                ArrayList<DeliveryTM> deliveryTMS = deliveryController.searchByID(txtSearch.getText());
                ObservableList<DeliveryTM>deliveryTMS1=FXCollections.observableArrayList();
                for (DeliveryTM deliveryTM : deliveryTMS) {
                    deliveryTMS1.add(new DeliveryTM(deliveryTM.getId(), deliveryTM.getName(), deliveryTM.getAddress(), deliveryTM.getContact(), deliveryTM.getSalary()));
                }
                tblDelivery.setItems(deliveryTMS1);
            }else{
                ArrayList<DeliveryTM> deliveryTMS = deliveryController.searchByName(txtSearch.getText());
                ObservableList<DeliveryTM>deliveryTMS1=FXCollections.observableArrayList();
                for (DeliveryTM deliveryTM : deliveryTMS) {
                    deliveryTMS1.add(new DeliveryTM(deliveryTM.getId(), deliveryTM.getName(), deliveryTM.getAddress(), deliveryTM.getContact(), deliveryTM.getSalary()));
                }
                tblDelivery.setItems(deliveryTMS1);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void selected(MouseEvent event) {
        txtName.setText(tblDelivery.getSelectionModel().getSelectedItem().getName());
        txtAddress.setText(tblDelivery.getSelectionModel().getSelectedItem().getAddress());
        txtContact.setText(String.valueOf(tblDelivery.getSelectionModel().getSelectedItem().getContact()));
        txtSalary.setText(String.valueOf(tblDelivery.getSelectionModel().getSelectedItem().getSalary()));

    }

    @FXML
    void update(ActionEvent event) {
        try {
            DeliveryController deliveryController=ServerConnector.getInstance().getDeliveryController();
            boolean update = deliveryController.update(new DeliveryTM(tblDelivery.getSelectionModel().getSelectedItem().getId(), txtName.getText(), txtAddress.getText(), Integer.parseInt(txtContact.getText()), Double.parseDouble(txtSalary.getText())));
            if(update){
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Updated Successfully");
                alert.show();
                loadTable();
            }else{
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setContentText("Update Failed");
                alert.show();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadTable(){
        try {
            DeliveryController deliveryController= ServerConnector.getInstance().getDeliveryController();
            ArrayList<DeliveryTM> all = deliveryController.getAll();
            ObservableList<DeliveryTM>deliveryTMS= FXCollections.observableArrayList();
            for (DeliveryTM deliveryTM : all) {
                deliveryTMS.add(new DeliveryTM(deliveryTM.getId(), deliveryTM.getName(), deliveryTM.getAddress(), deliveryTM.getContact(), deliveryTM.getSalary()));
            }
            tblDelivery.setItems(deliveryTMS);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tblDelivery.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("id"));
        tblDelivery.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("name"));
        tblDelivery.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("address"));
        tblDelivery.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("contact"));
        tblDelivery.getColumns().get(4).setCellValueFactory(new PropertyValueFactory<>("salary"));
        loadTable();
    }

    @FXML
    public void reload(ActionEvent actionEvent) {
        loadTable();
    }
}
