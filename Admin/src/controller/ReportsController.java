package controller;

import db.DBConnection;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import java.io.InputStream;
import java.net.URL;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;

public class ReportsController implements Initializable {

    @FXML
    void activeCheff(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/cheff.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void activeDeliverer(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/Delivery.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void activeReception(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/Reception.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void orders(ActionEvent event) {
        try {
            InputStream inputStream=getClass().getResourceAsStream("/Reports/Orders.jasper");
            HashMap hashMap=new HashMap();
            JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, hashMap, DBConnection.getConnection());
            JasperViewer.viewReport(jasperPrint,false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (JRException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
