package observerImpl;

import controller.ReceptionManagementController;
import dto.TableModel.ReceptionTM;
import observer.ReceptionObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ReceptionObserverImpl extends UnicastRemoteObject implements ReceptionObserver {

    private ReceptionManagementController receptionManagementController;

    public ReceptionObserverImpl(ReceptionManagementController receptionManagementController) throws RemoteException {
        this.receptionManagementController=receptionManagementController;
    }

    @Override
    public void update(ReceptionTM receptionTM) throws RemoteException {

    }
}
