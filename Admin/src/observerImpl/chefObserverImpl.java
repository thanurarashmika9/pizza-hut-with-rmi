package observerImpl;

import dto.CheffDTO;
import observer.ChefObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class chefObserverImpl extends UnicastRemoteObject implements ChefObserver {

    public chefObserverImpl() throws RemoteException {
    }

    @Override
    public void update(CheffDTO cheffDTO) throws RemoteException {

    }
}
