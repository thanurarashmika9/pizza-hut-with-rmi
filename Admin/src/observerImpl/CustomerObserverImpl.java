package observerImpl;

import controller.CustomerViewController;
import dto.CustomerDTO;
import observer.CustomerObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CustomerObserverImpl extends UnicastRemoteObject implements CustomerObserver {

    private CustomerViewController customerViewController;

    public CustomerObserverImpl(CustomerViewController customerViewController) throws RemoteException {
        this.customerViewController=customerViewController;
    }

    @Override
    public void update(CustomerDTO customerDTO) throws RemoteException {
        customerViewController.reload();
    }
}
