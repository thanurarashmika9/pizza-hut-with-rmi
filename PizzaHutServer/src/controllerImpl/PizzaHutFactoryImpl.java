package controllerImpl;

import controller.*;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class PizzaHutFactoryImpl extends UnicastRemoteObject implements PizzaHutFactory {
    public PizzaHutFactoryImpl()throws RemoteException{

    }

    @Override
    public CustomerController getCustomerController() throws RemoteException {
        return new CustomerControllerImpl();
    }

    @Override
    public ItemController getItemController() throws RemoteException {
        return new ItemControllerImpl();
    }

    @Override
    public OrderController getOrderController() throws RemoteException {
        return new OrderControllerImpl();
    }

    @Override
    public LoginController getLoginController() throws RemoteException {
        return new LoginControllerImpl();
    }

    @Override
    public Order_StatsController getOrder_StatsController() throws RemoteException {
        return new Order_StatsControllerImpl();
    }

    @Override
    public ChefController getChefController() throws RemoteException {
        return new ChefControllerImpl();
    }

    @Override
    public DeliveryController getDeliveryController() throws RemoteException {
        return new DeliveryControllerImpl();
    }

    @Override
    public ReceptionController getReceptionController() throws RemoteException {
        return new ReceptionControllerImpl();
    }
}
