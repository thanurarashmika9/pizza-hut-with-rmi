package controllerImpl;

import controller.ChefController;
import dbAccess.ChefDBAccess;
import dto.CheffDTO;
import dto.LoginDTO;
import observable.ChefObservable;
import observer.ChefObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ChefControllerImpl extends UnicastRemoteObject implements ChefController {

    private ChefDBAccess chefDBAccess=new ChefDBAccess();
    private static ChefObservable chefObservable=new ChefObservable();

    public ChefControllerImpl() throws RemoteException {
    }


    @Override
    public ArrayList<CheffDTO> searchCheff(String text) throws Exception {
        return chefDBAccess.searchCheff(text);
    }

    @Override
    public ArrayList<CheffDTO> viewChef() throws Exception {
        return chefDBAccess.getAllChef();
    }

    @Override
    public boolean addChef(CheffDTO cheffDTO) throws Exception {
        return chefDBAccess.newChef(cheffDTO);
    }

    @Override
    public boolean Update(CheffDTO cheffDTO) throws Exception {
        return chefDBAccess.update(cheffDTO);
    }

    @Override
    public ArrayList<CheffDTO> searchByName(String name) throws Exception {
        return chefDBAccess.searchByName(name);
    }

    @Override
    public boolean delete(String chid) throws Exception {
        return chefDBAccess.delete(chid);
    }

    @Override
    public void addChefObserver(ChefObserver chefObserver) throws RemoteException {
        chefObservable.addCustomer(chefObserver);
    }

    @Override
    public void removeChefObserver(ChefObserver chefObserver) throws RemoteException {
        chefObservable.removeCustomer(chefObserver);
    }

    @Override
    public ArrayList<LoginDTO> login() throws Exception {
        return chefDBAccess.login();
    }

    @Override
    public int getCount() {
        int count = chefObservable.getCount();
        return count;
    }
}
