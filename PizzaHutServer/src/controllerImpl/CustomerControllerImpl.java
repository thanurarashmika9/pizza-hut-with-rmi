package controllerImpl;

import controller.CustomerController;
import dbAccess.CustomerDBAccess;
import dto.CustomerDTO;
import observable.CustomerObservable;
import observer.CustomerObserver;
import reservation.CustomerReserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class CustomerControllerImpl extends UnicastRemoteObject implements CustomerController {

    private CustomerDBAccess customerDBAccess=new CustomerDBAccess();
    private static CustomerObservable customerObservable=new CustomerObservable();
    private static CustomerReserver customerReserver=new CustomerReserver();

    public CustomerControllerImpl()throws RemoteException {
    }


    @Override
    public boolean addCustomer(CustomerDTO customerDTO) throws Exception {
        boolean b = customerDBAccess.addCustomer(customerDTO);
        if(b){
            customerObservable.notifyObserver(customerDTO);
            return true;
        }return false;
    }

    @Override
    public boolean updateCustomer(CustomerDTO customerDTO) throws RemoteException {
        return false;
    }

    @Override
    public boolean deleteCustomer(String id) throws Exception {
        CustomerDTO customerDTO=searchcustomer(id);
        if(customerDBAccess.deleteCustomer(id)){
            customerObservable.notifyObserver(customerDTO);
            return true;
        }
        return false;
    }

    @Override
    public ArrayList<CustomerDTO> searchCustomer(String id) throws Exception {
        return customerDBAccess.searchCustomer(id);
    }

    @Override
    public ArrayList<CustomerDTO> getAllCustomers() throws Exception {
        return customerDBAccess.getAll();
    }

    @Override
    public ArrayList<CustomerDTO> searchCustomerbyName(String name) throws Exception {
        return customerDBAccess.searchCustomerbyName(name);
    }

    @Override
    public ArrayList<CustomerDTO> getDailyCustomers() throws Exception {
        return customerDBAccess.getDailyCustomers();
    }

    @Override
    public String getCID() throws Exception {
        return customerDBAccess.getCID();
    }

    @Override
    public CustomerDTO searchcustomer(String id) throws Exception {
        return customerDBAccess.searchcustomer(id);
    }

    @Override
    public boolean reserveCustomer(String id) throws RemoteException {
        return customerReserver.reserverCustomer(id,this);
    }

    @Override
    public boolean releaseCustomer(String id) throws RemoteException {
        return customerReserver.releaseCustomer(id,this);
    }

    @Override
    public void addCustomerObserver(CustomerObserver customerObserver) throws RemoteException {
        customerObservable.addCustomer(customerObserver);
    }

    @Override
    public void removeCustomerObserver(CustomerObserver customerObserver) throws RemoteException {
        customerObservable.removeCustomer(customerObserver);
    }
}
