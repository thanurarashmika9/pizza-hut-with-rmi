package controllerImpl;

import controller.ItemController;
import dbAccess.ItemDBAccess;
import dto.ItemDTO;
import dto.TableModel.ItemCategoryTM;
import dto.TableModel.ItemTM;
import javafx.collections.ObservableList;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.SQLException;
import java.util.ArrayList;

public class ItemControllerImpl extends UnicastRemoteObject implements ItemController {

    private ItemDBAccess itemDBAccess=new ItemDBAccess();

    public ItemControllerImpl()throws RemoteException {

    }

    @Override
    public ArrayList<ItemDTO> getItems() throws Exception {
        return itemDBAccess.getItems();
    }

    @Override
    public String getItemID(String name) throws Exception {
        return itemDBAccess.getItemID(name);
    }

    @Override
    public boolean addItem(ItemDTO itemDTO) throws RemoteException {
        return false;
    }

    @Override
    public boolean updateItem(ItemDTO itemDTO) throws RemoteException {
        return false;
    }

    @Override
    public boolean deleteItem(String iid) throws RemoteException {
        return false;
    }

    @Override
    public ItemDTO searchItem(String iid) throws RemoteException {
        return null;
    }

    @Override
    public ItemDTO searchItemByName(String name) throws RemoteException {
        return null;
    }

    @Override
    public double getItemPrice(String name) throws Exception {
        return itemDBAccess.getPrice(name);
    }

    @Override
    public ArrayList<ItemCategoryTM> getCategory() throws Exception {
        return itemDBAccess.getCategory();
    }

    @Override
    public ArrayList<ItemDTO> getAllItems() throws Exception {
        return itemDBAccess.getAllItems();
    }

    @Override
    public ArrayList<ItemDTO> searchItemData(String text) throws Exception {
        return itemDBAccess.searchItemData(text);
    }

    @Override
    public String getItemName(String iid) throws Exception {
        return itemDBAccess.getItemName(iid);
    }

    @Override
    public ArrayList<ItemDTO> getItem(String iid) throws Exception {
        return itemDBAccess.search(iid);
    }
}
