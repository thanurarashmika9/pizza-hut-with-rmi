package controllerImpl;

import controller.LoginController;
import dbAccess.LoginDBAccess;
import dto.LoginDTO;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class LoginControllerImpl extends UnicastRemoteObject implements LoginController {

    private LoginDBAccess loginDBAccess=new LoginDBAccess();

    public LoginControllerImpl()throws RemoteException{
      //  loginDBAccess=this;
    }

    @Override
    public ArrayList<LoginDTO> getSecurityData() throws Exception {
        return loginDBAccess.getSecurityData();

    }

    @Override
    public LoginDTO checkSecurity(String nameText) throws Exception {
        return loginDBAccess.check(nameText);
    }

    @Override
    public ArrayList<LoginDTO> check() throws Exception {
        return loginDBAccess.login();
    }

    @Override
    public ArrayList<LoginDTO> login() throws Exception {
        return loginDBAccess.adminLogin();
    }

    /*@Override
    public ArrayList<LoginDTO> getAllLogingData() throws Exception {
        return loginDBAccess.getAllLoginData();
    }*/
}
