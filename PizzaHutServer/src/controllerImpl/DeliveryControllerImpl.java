package controllerImpl;

import controller.DeliveryController;
import dbAccess.DeliveryDBAccess;
import dto.TableModel.DeliveryTM;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class DeliveryControllerImpl extends UnicastRemoteObject implements DeliveryController {

    private DeliveryDBAccess deliveryDBAccess=new DeliveryDBAccess();

    public DeliveryControllerImpl() throws RemoteException {
    }

    @Override
    public ArrayList<DeliveryTM> getAll() throws Exception {
        return deliveryDBAccess.getAll();
    }

    @Override
    public ArrayList<DeliveryTM>searchByID(String did) throws Exception {
        return deliveryDBAccess.searchByID(did);
    }

    @Override
    public ArrayList<DeliveryTM> searchByName(String name) throws Exception {
        return deliveryDBAccess.searchByName(name);
    }

    @Override
    public boolean update(DeliveryTM deliveryTM) throws Exception {
        return deliveryDBAccess.update(deliveryTM);
    }

    @Override
    public boolean add(DeliveryTM deliveryTM) throws Exception {
        return deliveryDBAccess.add(deliveryTM);
    }

    @Override
    public boolean delete(String id) throws Exception {
        return deliveryDBAccess.delete(id);
    }
}
