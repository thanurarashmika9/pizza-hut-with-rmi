package controllerImpl;

import controller.ReceptionController;
import dbAccess.ReceptionDBAccess;
import dto.ReceptionDTO;
import dto.TableModel.ReceptionTM;
import observable.ReceptionObservable;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class ReceptionControllerImpl extends UnicastRemoteObject implements ReceptionController {

    private ReceptionDBAccess receptionDBAccess=new ReceptionDBAccess();
    private static ReceptionObservable receptionObservable=new ReceptionObservable();

    public ReceptionControllerImpl() throws RemoteException {
    }

    @Override
    public boolean add(ReceptionTM receptionTM) throws Exception {
        return receptionDBAccess.add(receptionTM);
    }

    @Override
    public boolean deleteItem(String rid) throws Exception {
        return receptionDBAccess.delete(rid);
    }

    @Override
    public ArrayList<ReceptionDTO> getAll() throws Exception {
        return receptionDBAccess.getAll();
    }

    @Override
    public ArrayList<ReceptionDTO> searchByID(String id) throws Exception {
        return receptionDBAccess.searchByID(id);
    }

    @Override
    public ArrayList<ReceptionDTO> searchByName(String name) throws Exception {
        return receptionDBAccess.searchByName(name);
    }

    @Override
    public boolean Update(ReceptionDTO receptionDTO) throws Exception {
        return receptionDBAccess.update(receptionDTO);
    }
}
