package controllerImpl;

import controller.OrderController;
import dbAccess.OrderDBAccess;
import dto.DeliveryDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.DeliveryInfoTM;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class OrderControllerImpl extends UnicastRemoteObject implements OrderController {

    private OrderDBAccess orderDBAccess=new OrderDBAccess();
    public ArrayList<OrderDTO>orders=new ArrayList<>();

    public OrderControllerImpl()throws RemoteException{

    }

    @Override
    public boolean makeOrder(OrderDTO orderDTO) throws Exception {
        return orderDBAccess.makeOrder(orderDTO);
    }

    @Override
    public ArrayList<OrderDTO> orders(String date) throws Exception {
        return orderDBAccess.getOrders(date);
    }

    @Override
    public OrderDTO searchOrder(String oid) throws RemoteException {
        return null;
    }

    @Override
    public ArrayList<OrderDTO> getOrders() throws Exception {
        return orderDBAccess.getAll();
    }

    @Override
    public String getOID() throws Exception {
        return orderDBAccess.getOID();
    }

    @Override
    public OrderDTO getOrder() throws RemoteException {
        return orderDBAccess.getNewOrder();
    }

    @Override
    public int getOrderCount() throws RemoteException{
        return orderDBAccess.getOrderCount();
    }

    @Override
    public ArrayList<OrderDTO> searchByOID(String order) throws Exception {
        return orderDBAccess.searchByOID(order);
    }

    @Override
    public ArrayList<OrderDTO> searchByCID(String cid) throws Exception {
        return orderDBAccess.searchByCID(cid);
    }

    @Override
    public ArrayList<OrderDTO> searchByIID(String iid) throws Exception {
        return orderDBAccess.searchByIID(iid);
    }

    @Override
    public boolean saveKitchenOrder(OrderDTO order) throws Exception {
        return orderDBAccess.saveKitchenOrder(order);
    }

    @Override
    public ArrayList<DeliveryDTO> getDeliveryBoys() throws Exception {
        return orderDBAccess.getDeliveryBoys();
    }

    @Override
    public ArrayList<DeliveryInfoTM> getDeliveryStats() throws Exception {
        return orderDBAccess.getDeliveryData();
    }

    @Override
    public boolean updateOrder(String oid,String Deliver) throws Exception {
        return orderDBAccess.updateDeliveryInfo(oid,Deliver);
    }

    @Override
    public String getDid(String oid) throws Exception {
        return orderDBAccess.getDid(oid);
    }

    @Override
    public boolean updateCheff(String oid, String chid) throws Exception {
        return orderDBAccess.updateOrder(oid,chid);
    }


}
