package controllerImpl;

import controller.Order_StatsController;
import dbAccess.Order_StatsDBAccess;
import dto.Order_StatsDTO;
import observable.CustomerObservable;
import observable.Order_StatObservable;
import observer.Order_StatObserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class Order_StatsControllerImpl extends UnicastRemoteObject implements Order_StatsController {

    private Order_StatsDBAccess order_statsDBAccess=new Order_StatsDBAccess();
    private static Order_StatObservable order_statObservable=new Order_StatObservable();


    public Order_StatsControllerImpl() throws RemoteException {
    }


    @Override
    public ArrayList<Order_StatsDTO> getData() throws Exception {
        return order_statsDBAccess.getData();
    }

    @Override
    public boolean addData(Order_StatsDTO order_statsDTO) throws Exception {
        boolean b=order_statsDBAccess.addData(order_statsDTO);
        if(b){
            order_statObservable.notifyObserver(order_statsDTO);
            return true;
        }
        return false;
    }

    @Override
    public void addCustomerObserver(Order_StatObserver order_statObserver) throws RemoteException {
        order_statObservable.addCustomer(order_statObserver);
    }

    @Override
    public void removeCustomerObserver(Order_StatObserver order_statObserver) throws RemoteException {
        order_statObservable.removeCustomer(order_statObserver);
    }

    @Override
    public boolean updateTbl(String state,String oid) throws Exception {
        boolean b = order_statsDBAccess.finishOrder(state, oid);
        if(b){
            order_statObservable.notifyObserver(new Order_StatsDTO(oid,state));
            return true;
        }
        return false;
    }
}
