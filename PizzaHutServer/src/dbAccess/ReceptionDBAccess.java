package dbAccess;

import dto.ReceptionDTO;
import dto.TableModel.ReceptionTM;
import util.CrudUtil;

import java.sql.ResultSet;
import java.util.ArrayList;

public class ReceptionDBAccess {
    public boolean delete(String rid) throws Exception {
        return CrudUtil.executeUpdate("Delete From Reception where rid=?",rid);
    }

    public boolean add(ReceptionTM receptionTM) throws Exception {
        return CrudUtil.executeUpdate("Insert Into Reception Values(?,?,?,?,?)",receptionTM.getRid(),receptionTM.getName(),receptionTM.getAddress(),receptionTM.getContact(),receptionTM.getSalary());
    }

    public ArrayList<ReceptionDTO> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Reception");
        ArrayList<ReceptionDTO>receptionDTOS=new ArrayList<>();
        while (rst.next()) {
            receptionDTOS.add(new ReceptionDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return receptionDTOS;
    }

    public ArrayList<ReceptionDTO> searchByID(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Reception where rid=?", id);
        ArrayList<ReceptionDTO>receptionDTOS=new ArrayList<>();
        while (rst.next()) {
            receptionDTOS.add(new ReceptionDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return receptionDTOS;
    }

    public ArrayList<ReceptionDTO> searchByName(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Reception where name=?", name);
        ArrayList<ReceptionDTO>receptionDTOS=new ArrayList<>();
        while (rst.next()) {
            receptionDTOS.add(new ReceptionDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return receptionDTOS;
    }

    public boolean update(ReceptionDTO receptionDTO) throws Exception {
        return CrudUtil.executeUpdate("Update Reception set name=?,address=?,contact=?,salary=? where rid=?",receptionDTO.getName(),receptionDTO.getAddress(),receptionDTO.getContact(),receptionDTO.getSalary(),receptionDTO.getRid());
    }
}
