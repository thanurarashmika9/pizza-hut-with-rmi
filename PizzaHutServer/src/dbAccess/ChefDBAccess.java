package dbAccess;

import dto.CheffDTO;
import dto.LoginDTO;
import util.CrudUtil;

import java.sql.ResultSet;
import java.util.ArrayList;

public class ChefDBAccess {
    public ArrayList<CheffDTO> searchCheff(String text) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Cheff where chid=?", text);
        ArrayList<CheffDTO>cheffDTOS=new ArrayList<>();
        while (rst.next()) {
            cheffDTOS.add(new CheffDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return cheffDTOS;
    }

    public ArrayList<CheffDTO> getAllChef() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Cheff");
        ArrayList<CheffDTO>cheffDTOS=new ArrayList<>();
        while(rst.next()){
            cheffDTOS.add(new CheffDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return cheffDTOS;
    }

    public boolean newChef(CheffDTO cheffDTO) throws Exception {
        return CrudUtil.executeUpdate("Insert Into Cheff Values(?,?,?,?,?)",cheffDTO.getChid(),cheffDTO.getName(),cheffDTO.getAddress(),cheffDTO.getContact(),cheffDTO.getSalary());
    }

    public boolean update(CheffDTO cheffDTO) throws Exception {
        return CrudUtil.executeUpdate("Update Cheff set name=?,address=?,contact=?,salary=? where chid=?",cheffDTO.getName(),cheffDTO.getAddress(),cheffDTO.getContact(),cheffDTO.getSalary(),cheffDTO.getChid());
    }

    public ArrayList<CheffDTO> searchByName(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Cheff where name=?", name);
        ArrayList<CheffDTO>cheffDTOS=new ArrayList<>();
        while (rst.next()) {
            cheffDTOS.add(new CheffDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return cheffDTOS;
    }

    public boolean delete(String chid) throws Exception {
        return CrudUtil.executeUpdate("Delete From Cheff where chid=?",chid);
    }

    public ArrayList<LoginDTO> login() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * From Chef_Login");
        ArrayList<LoginDTO>loginDTOS=new ArrayList<>();
        while (rst.next()) {
            loginDTOS.add(new LoginDTO(rst.getString(1),rst.getString(2)));
        }
        return loginDTOS;
    }
}
