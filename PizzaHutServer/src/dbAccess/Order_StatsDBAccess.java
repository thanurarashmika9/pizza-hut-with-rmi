package dbAccess;

import dto.Order_StatsDTO;
import util.CrudUtil;

import java.sql.ResultSet;
import java.util.ArrayList;

public class Order_StatsDBAccess {

    public ArrayList<Order_StatsDTO> getData() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select os.oid,os.state from Order_Stats os,Orders o where os.oid=o.oid && o.oDate=CurDate()");
        ArrayList<Order_StatsDTO>order_statsDTOS=new ArrayList<>();
        while (rst.next()) {
            order_statsDTOS.add(new Order_StatsDTO(rst.getString(1),rst.getString(2)));
        }
        return order_statsDTOS;
    }

    public boolean addData(Order_StatsDTO order_statsDTO) throws Exception {
        return CrudUtil.executeUpdate("Insert Into Order_Stats Values(?,?)",order_statsDTO.getOid(),order_statsDTO.getStatus());
    }

    public boolean finishOrder(String state,String oid) throws Exception {
        return CrudUtil.executeUpdate("Update Order_Stats set state=? where oid=?",state,oid);
    }
}
