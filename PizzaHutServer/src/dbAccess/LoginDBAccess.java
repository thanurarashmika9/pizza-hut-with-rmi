package dbAccess;

import dto.LoginDTO;
import util.CrudUtil;

import java.sql.ResultSet;
import java.util.ArrayList;

public class LoginDBAccess {

    public ArrayList<LoginDTO>getSecurityData() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from login");
        ArrayList<LoginDTO>loginDTOS=new ArrayList<>();
        while (rst.next()) {
            loginDTOS.add(new LoginDTO(rst.getString(1),rst.getString(2)));
        }
        return loginDTOS;
    }

    public LoginDTO check(String nameText) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select * from Reception_Login");
        while (rst.next()) {
            if(nameText==rst.getString(2)){
                System.out.println(rst.getString(2));
                return new LoginDTO(rst.getString(2),rst.getString(3));
            }
        }
        return null;
    }

    public ArrayList<LoginDTO> login() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Reception_Login");
        ArrayList<LoginDTO>loginDTOS=new ArrayList<>();
        while (rst.next()) {
            loginDTOS.add(new LoginDTO(rst.getString(1),rst.getString(2)));
        }
        return loginDTOS;
    }

    public ArrayList<LoginDTO> adminLogin() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select * from admin_login");
        ArrayList<LoginDTO>loginDTOS=new ArrayList<>();
        while (rst.next()) {
            loginDTOS.add(new LoginDTO(rst.getString(1),rst.getString(2)));
        }
        return loginDTOS;
    }

    /*public ArrayList<LoginDTO> getAllLoginData() throws Exception {
        ResultSet rst=CrudUtil.executeQuery("Select * from login");
        ArrayList<LoginDTO>loginDTOS=new ArrayList<>();
        while (rst.next()){
            loginDTOS.add(new LoginDTO(rst.getString(1),rst.getString(2)));
        }
        System.out.println(loginDTOS.indexOf(1));
        return loginDTOS;

    }*/
}
