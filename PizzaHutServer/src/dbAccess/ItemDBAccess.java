package dbAccess;

import dto.ItemDTO;
import dto.TableModel.ItemCategoryTM;
import util.CrudUtil;

import java.sql.*;
import java.util.ArrayList;

public class ItemDBAccess {


    public ArrayList<ItemDTO> getItems() throws Exception {

        ResultSet rst=CrudUtil.executeQuery( "SELECT Description FROM Item");
        ArrayList<ItemDTO>names=new ArrayList<>();
        while (rst.next()) {
            names.add(new ItemDTO(rst.getString(1)));
        }
        return names;
    }

    public ItemDTO searchItem(String iid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Item where iid=?", iid);
        return new ItemDTO(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getString(4));
    }

    public double getPrice(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select unit_price from Item where description=?", name);
        double unitPrice=0;
        if(rst.next()){
            unitPrice=rst.getDouble(1);
        }
        return unitPrice;
    }

    public ArrayList<ItemCategoryTM> getCategory() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Category");
        ArrayList<ItemCategoryTM>itemCategoryTMS=new ArrayList<>();
        while (rst.next()) {
            itemCategoryTMS.add(new ItemCategoryTM(rst.getString(1),rst.getString(2)));
        }
        return itemCategoryTMS;
    }

    public ArrayList<ItemDTO> getAllItems() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Item");
        ArrayList<ItemDTO>itemDTOS=new ArrayList<>();
        while (rst.next()) {
            itemDTOS.add(new ItemDTO(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getString(4)));
        }
        return itemDTOS;
    }

    public ArrayList<ItemDTO> searchItemData(String text) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select * from Item Where iid=?", text);
        ArrayList<ItemDTO>itemDTOS=new ArrayList<>();
        if (rst.next()) {
            itemDTOS.add(new ItemDTO(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getString(4)));
        }
        return itemDTOS;
    }

    public String getItemID(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select iid from Item Where description=?", name);
        String iid=null;
        if (rst.next()) {
            iid=rst.getString(1);
        }
        return iid;
    }

    public String getItemName(String iid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select description from Item where iid=?", iid);
        String item=null;
        if (rst.next()) {
            item=rst.getString(1);
        }
        return item;
    }

    public ArrayList<ItemDTO> search(String iid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Item where description=?", iid);
        ArrayList<ItemDTO>itemDTOS=new ArrayList<>();
        while (rst.next()) {
            itemDTOS.add(new ItemDTO(rst.getString(1),rst.getString(2),rst.getDouble(3),rst.getString(4)));
        }
        return itemDTOS;
    }
}
