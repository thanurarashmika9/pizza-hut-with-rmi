package dbAccess;

import dto.TableModel.DeliveryTM;
import util.CrudUtil;

import java.sql.ResultSet;
import java.util.ArrayList;

public class DeliveryDBAccess {
    public ArrayList<DeliveryTM> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Deliver");
        ArrayList<DeliveryTM>deliveryTMS=new ArrayList<>();
        while (rst.next()) {
            deliveryTMS.add(new DeliveryTM(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return deliveryTMS;
    }

    public ArrayList<DeliveryTM> searchByID(String did) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Deliver where did=?", did);
        ArrayList<DeliveryTM>deliveryTMS=new ArrayList<>();
        while (rst.next()) {
            deliveryTMS.add(new DeliveryTM(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return deliveryTMS;
    }

    public ArrayList<DeliveryTM> searchByName(String name) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Deliver where name=?", name);
        ArrayList<DeliveryTM>deliveryTMS=new ArrayList<>();
        while (rst.next()) {
            deliveryTMS.add(new DeliveryTM(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4),rst.getDouble(5)));
        }
        return deliveryTMS;
    }

    public boolean update(DeliveryTM deliveryTM) throws Exception {
        return CrudUtil.executeUpdate("Update Deliver set name=?,address=?,contact=?,salary=? where did=?",deliveryTM.getName(),deliveryTM.getAddress(),deliveryTM.getContact(),deliveryTM.getSalary(),deliveryTM.getId());
    }

    public boolean add(DeliveryTM deliveryTM) throws Exception {
        return CrudUtil.executeUpdate("Insert Into Deliver values(?,?,?,?,?)",deliveryTM.getId(),deliveryTM.getName(),deliveryTM.getAddress(),deliveryTM.getContact(),deliveryTM.getSalary());
    }

    public boolean delete(String id) throws Exception {
        return CrudUtil.executeUpdate("Delete from Deliver where did=?",id);
    }
}
