package dbAccess;

import db.DBConnection;
import dto.CustomerDTO;
import javafx.collections.FXCollections;
import util.CrudUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerDBAccess {

    public boolean addCustomer(CustomerDTO customerDTO) throws Exception {
        return CrudUtil.executeUpdate("Insert Into Customer Values(?,?,?,?)", customerDTO.getCid(), customerDTO.getName(), customerDTO.getAddress(), customerDTO.getContact());
    }

    public boolean deleteCustomer(String cid) throws Exception {
        boolean rst = CrudUtil.executeUpdate("Delete from Customer where cid=?", cid);
        return rst;
    }

    public boolean updateCustomer(CustomerDTO customerDTO)throws Exception{
        boolean rst = CrudUtil.executeUpdate("Update Customer Set cid=?,name=?,address=?,contact=?", customerDTO.getCid(), customerDTO.getName(), customerDTO.getAddress(), customerDTO.getContact());
        return rst;
    }

    public ArrayList<CustomerDTO> searchCustomer(String id)throws Exception{
        ResultSet rst = CrudUtil.executeQuery("Select * from Customer Where cid=?", id);
        ArrayList<CustomerDTO>customerDTOS = new ArrayList<>();
        if(rst.next()) {
            customerDTOS.add(new CustomerDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4)));
        }
        return customerDTOS;
    }

    public ArrayList<CustomerDTO>customers() throws Exception {
        ArrayList<CustomerDTO>customerDTOS=new ArrayList<>();
        ResultSet rst = CrudUtil.executeQuery("Select * from Customer");
        while (rst.next()) {
            customerDTOS.add(new CustomerDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4)));
        }
        return customerDTOS;
    }

    public ArrayList<CustomerDTO> searchCustomerbyName(String name) throws Exception {
        ArrayList<CustomerDTO>customerDTOS=new ArrayList<>();
        ResultSet rst = CrudUtil.executeQuery("Select * from Customer where name=?", name);
        while (rst.next()) {
            customerDTOS.add(new CustomerDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4)));
        }
        return customerDTOS;
    }

    public ArrayList<CustomerDTO>getAll() throws Exception {
        ArrayList<CustomerDTO>customerDTOS=new ArrayList<>();
        ResultSet rst = CrudUtil.executeQuery("Select * from Customer");
        while (rst.next()) {
            customerDTOS.add(new CustomerDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4)));
        }
        return customerDTOS;
    }

    public ArrayList<CustomerDTO> getDailyCustomers() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select c.cid,c.name from Customer c,Orders o where o.cid=c.cid && o.oDate=curDate()");
        ArrayList<CustomerDTO>customerDTOS=new ArrayList<>();
        while (rst.next()) {
            customerDTOS.add(new CustomerDTO(rst.getString(1),rst.getString(2)));
        }
        return customerDTOS;
    }

    public String getCID() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select cid From Customer Order by 1 desc Limit 1");
        String cid=null;
        if (rst.next()) {
            cid=rst.getString(1);
        }
        return cid;
    }

    public CustomerDTO searchcustomer(String id) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Customer Where cid=?", id);
        CustomerDTO customerDTO=new CustomerDTO(rst.getString(1),rst.getString(2),rst.getString(3),rst.getInt(4));
        return customerDTO;
    }
}
