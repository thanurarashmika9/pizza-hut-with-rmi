package dbAccess;

import dto.DeliveryDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import dto.TableModel.DeliveryInfoTM;
import util.CrudUtil;

import java.rmi.RemoteException;
import java.sql.ResultSet;
import java.util.ArrayList;

public class OrderDBAccess {

    public static ArrayList<OrderDTO>orderDTOS=new ArrayList<>();

    public boolean makeOrder(OrderDTO orderDTO) throws Exception {
        boolean b = CrudUtil.executeUpdate("Insert Into Orders values(?,?,?,?,?,?,?,?,?,?)", orderDTO.getOid(), orderDTO.getoDate(), orderDTO.getCid(), orderDTO.getIid(), orderDTO.getPrice(), orderDTO.getQty(), orderDTO.getRid(), orderDTO.getChid(), orderDTO.getDid(), orderDTO.getTotal());
        orderDTOS.add(orderDTO);
        return b;
    }

    public OrderDTO getNewOrder(){
        if(orderDTOS.size()>0) {
            OrderDTO order = orderDTOS.get(0);
            orderDTOS.remove(0);
            return order;
        }else{
            return null;
        }
    }

    public ArrayList<OrderDTO> getOrders(String date) throws Exception {
        ArrayList<OrderDTO>orderDTOS=new ArrayList<>();
        ResultSet rst = CrudUtil.executeQuery("Select * from Orders where oDate=?",date);
        while (rst.next()) {
            orderDTOS.add(new OrderDTO(rst.getString(1),rst.getDate(2),rst.getString(3),rst.getString(4),rst.getDouble(5),rst.getInt(6),rst.getString(7),rst.getString(8),rst.getString(9),rst.getDouble(10)));
        }
        return orderDTOS;
    }

    public ArrayList<OrderDTO> getAll() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Orders");
        ArrayList<OrderDTO>orderDTOS=new ArrayList<>();
        while (rst.next()) {
            orderDTOS.add(new OrderDTO(rst.getString(1),rst.getDate(2),rst.getString(3),rst.getString(4),rst.getDouble(5),rst.getInt(6),rst.getString(7),rst.getString(8),rst.getString(9),rst.getDouble(10)));
        }
        return orderDTOS;
    }

    public String getOID() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select oid from Orders Order by oid desc limit 1");
        String oid=null;
        if (rst.next()) {
            oid=rst.getString(1);
        }
        return oid;
    }

    public int getOrderCount() throws RemoteException {
        int size = orderDTOS.size();
        return size;
    }

    public ArrayList<OrderDTO> searchByOID(String order) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Orders where oid=?", order);
        ArrayList<OrderDTO>orderDTOS=new ArrayList<>();
        if (rst.next()) {
            orderDTOS.add(new OrderDTO(rst.getString(1),rst.getDate(2),rst.getString(3),rst.getString(4),rst.getDouble(5),rst.getInt(6),rst.getString(7),rst.getString(8),rst.getString(9),rst.getDouble(10)));
        }
        return orderDTOS;
    }

    public ArrayList<OrderDTO> searchByIID(String iid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Orders where iid=?", iid);
        ArrayList<OrderDTO>orderDTOS=new ArrayList<>();
        while (rst.next()) {
            orderDTOS.add(new OrderDTO(rst.getString(1),rst.getDate(2),rst.getString(3),rst.getString(4),rst.getDouble(5),rst.getInt(6),rst.getString(7),rst.getString(8),rst.getString(9),rst.getDouble(10)));
        }
        return orderDTOS;
    }

    public ArrayList<OrderDTO> searchByCID(String cid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Orders where cid=?", cid);
        ArrayList<OrderDTO>orderDTOS=new ArrayList<>();
        while (rst.next()) {
            orderDTOS.add(new OrderDTO(rst.getString(1),rst.getDate(2),rst.getString(3),rst.getString(4),rst.getDouble(5),rst.getInt(6),rst.getString(7),rst.getString(8),rst.getString(9),rst.getDouble(10)));
        }
        return orderDTOS;
    }

    public boolean saveKitchenOrder(OrderDTO order) throws Exception {
        System.out.println(order);
        return CrudUtil.executeUpdate("Insert Into Kitchen_Records values(?,?,?,?,?,?,?,?,?,?)", order.getOid(), order.getoDate(), order.getCid(), order.getIid(), order.getPrice(), order.getQty(), order.getRid(), order.getChid(), order.getDid(), order.getTotal());
    }

    public ArrayList<DeliveryDTO> getDeliveryBoys() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("Select * from Deliver");
        ArrayList<DeliveryDTO>deliveryDTOS=new ArrayList<>();
        while(rst.next()){
            deliveryDTOS.add(new DeliveryDTO(rst.getString(1)));
        }
        return deliveryDTOS;
    }

    public ArrayList<DeliveryInfoTM> getDeliveryData() throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select oid,did from Orders where did=?", "Delivering");
        ArrayList<DeliveryInfoTM>deliveryInfoTMS=new ArrayList<>();
        while (rst.next()) {
            deliveryInfoTMS.add(new DeliveryInfoTM(rst.getString(1),rst.getString(2)));
        }
        return deliveryInfoTMS;
    }

    public boolean updateOrder(String oid,String cheff) throws Exception {
        return CrudUtil.executeUpdate("Update Orders set chid=? where oid=?",cheff,oid);
    }

    public boolean updateDeliveryInfo(String oid, String Deliver) throws Exception {
        return CrudUtil.executeUpdate("Update Orders set did=? where oid=?",Deliver,oid);
    }

    public String getDid(String oid) throws Exception {
        ResultSet rst = CrudUtil.executeQuery("select did from Orders where oid=?", oid);
        String did=null;
        if (rst.next()) {
            did=rst.getString(1);
        }
        return did;
    }

/*    public void placeOrder(OrderDTO recDemo) throws Exception {
        CrudUtil.executeUpdate("Insert Into Orders Values(?,?,?,?,?,?,?,?,?,?)",recDemo.getOid(),recDemo.getoDate(),recDemo.getCid(),recDemo.getIid(),recDemo.getPrice(),recDemo.getQty(),recDemo.getRid(),recDemo.getChid(),recDemo.getDid(),recDemo.getTotal());
    }*/
}
