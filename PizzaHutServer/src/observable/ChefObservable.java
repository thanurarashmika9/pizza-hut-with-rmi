package observable;

import dto.CheffDTO;
import observer.ChefObserver;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class ChefObservable {
    private static ArrayList<ChefObserver>pizzaHutObservers = new ArrayList<>();

    public void addCustomer(ChefObserver chefObserver){
        pizzaHutObservers.add(chefObserver);
    }

    public void removeCustomer(ChefObserver chefObserver){
        pizzaHutObservers.remove(chefObserver);
    }

    public void notifyObserver(CheffDTO cheffDTO)throws RemoteException {
        for(ChefObserver observer : pizzaHutObservers){
            observer.update(cheffDTO);
        }
    }

    public int getCount(){
        int size = pizzaHutObservers.size();
        return size;
    }
}
