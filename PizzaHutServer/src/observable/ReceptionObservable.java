package observable;

import dto.TableModel.ReceptionTM;
import observer.ReceptionObserver;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class ReceptionObservable {
    private static ArrayList<ReceptionObserver> pizzaHutObservers = new ArrayList<>();

    public void addReception(ReceptionObserver receptionObserver){
        pizzaHutObservers.add(receptionObserver);
    }

    public void removeReception(ReceptionObserver receptionObserver){
        pizzaHutObservers.remove(receptionObserver);
    }

    public void notifyObserver(ReceptionTM receptionTM)throws RemoteException {
        for(ReceptionObserver observer : pizzaHutObservers){
            observer.update(receptionTM);
        }
    }
}
