package observable;

import dto.CustomerDTO;
import dto.OrderDTO;
import dto.Order_StatsDTO;
import observer.CustomerObserver;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class CustomerObservable {
    private static ArrayList<CustomerObserver>pizzaHutObservers = new ArrayList<>();

    public void addCustomer(CustomerObserver customerObserver){
        pizzaHutObservers.add(customerObserver);
    }

    public void removeCustomer(CustomerObserver customerObserver){
        pizzaHutObservers.remove(customerObserver);
    }

    public void notifyObserver(CustomerDTO customerDTO)throws RemoteException{
        for(CustomerObserver observer : pizzaHutObservers){
            observer.update(customerDTO);
        }
    }
}
