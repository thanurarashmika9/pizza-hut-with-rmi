package observable;

import dto.Order_StatsDTO;
import observer.Order_StatObserver;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class Order_StatObservable {
    private static ArrayList<Order_StatObserver> pizzaHutObservers = new ArrayList<>();

    public void addCustomer(Order_StatObserver order_statObserver){
        pizzaHutObservers.add(order_statObserver);
    }

    public void removeCustomer(Order_StatObserver order_statObserver){
        pizzaHutObservers.remove(order_statObserver);
    }

    public void notifyObserver(Order_StatsDTO order_statsDTO)throws RemoteException {
        for(Order_StatObserver observer : pizzaHutObservers){
            observer.update(order_statsDTO);
        }
    }
}
