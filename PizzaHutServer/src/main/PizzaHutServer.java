package main;

import controllerImpl.PizzaHutFactoryImpl;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class PizzaHutServer {
    public static void main(String[] args) {
        try {
            Registry PizzaHutRegistry = LocateRegistry.createRegistry(5050);
            System.out.println("Server is Starting");
            PizzaHutRegistry.rebind("PizzaServer",new PizzaHutFactoryImpl());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}




